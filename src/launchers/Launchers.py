import copy
import logging
import multiprocessing
import os
import shutil
from multiprocessing import Queue

from src.helpers.FolderHelper import FolderHelper
from src.helpers.LoggingHelper import LoggingHelper
from src.helpers.PredictionHelper import PredictionHelper
from src.detectors.Detectors import XSSGeneratorConcatenationDetector, XSSConcatenationDetector
from src.configurations.dfs_name import *
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper


class GenericLauncher(LoggingHelper):

    def __init__(self, config):
        super().__init__()
        self.logger = logging.getLogger(__name__)
        self.config = config
        for k, v in self.config.get_params_of(GenericLauncher.__name__).items() :
            self.__setattr__(k,v)
        self.lang = self.config.get_current_lang()
        self.settup_logging(GenericLauncher.__name__)
        self.XSSConcatenationDetector = None
        self.global_res = {DF_LOSS_TRAINING: {},
                           DF_ACCURACY_TRAINING: {},
                           DF_LOSS_EVALUATION: {},
                           DF_ACCURACY_EVALUATION: {},
                           DF_PRECISION_EVALUATION: {},
                           DF_RECALL_EVALUATION: {},
                           DF_F_MEASURED_EVALUATION: {},
                           DF_TN_EVALUATION: {},
                           DF_TP_EVALUATION: {},
                           DF_FN_EVALUATION: {},
                           DF_FP_EVALUATION: {},
                           DF_WORD2VEC_TIME: {},
                           DF_PADDING_TIME: {},
                           DF_PARTITION_TIME: {},
                           DF_KERAS_TRAINING_TIME: {},
                           DF_KERAS_EVALUATION_TIME: {},
                           DF_GLOBAL_TIME: {}
                           }
        self.global_test = None
        self.list_df = [DF_ACCURACY_TRAINING, DF_LOSS_TRAINING, DF_ACCURACY_EVALUATION, DF_LOSS_EVALUATION,
                        DF_PRECISION_EVALUATION, DF_RECALL_EVALUATION, DF_F_MEASURED_EVALUATION,
                        DF_TN_EVALUATION, DF_TP_EVALUATION, DF_FN_EVALUATION, DF_FP_EVALUATION, DF_WORD2VEC_TIME,
                        DF_PADDING_TIME, DF_KERAS_TRAINING_TIME, DF_KERAS_EVALUATION_TIME,
                        DF_GLOBAL_TIME, DF_PARTITION_TIME]
        self.dfs = None
        self.dfs_test = None
        self.df_prediction = None


    def set_detector(self, detector_class_name):
        self.XSSConcatenationDetector =  eval(detector_class_name)(self.config)
        if "Generator" in detector_class_name :
            self.generator_usage_predicate = True
        else :
            self.generator_usage_predicate = False

    def choose_detector(self):
        if self.generator_usage_predicate:
            self.set_detector(XSSGeneratorConcatenationDetector.__name__)
        else:
            self.set_detector(XSSConcatenationDetector.__name__)



    def setup(self, **kwargs):
        for k, v in kwargs.items() :
            setattr(self, k, v)
        # pprint(self.__dict__)
        self.store_env = kwargs
        for df in self.global_res:
            for embed in self.embedding_sizes:
                self.global_res[df][embed] = {}
        self.dfs = ResultDataframeFilesHelper.createDataframes(len(self.list_df), index=self.embedding_sizes, columns=self.keras_layers)
        self.dfs_test = ResultDataframeFilesHelper.createDataframes(len(self.list_df), index=self.embedding_sizes, columns=self.keras_layers)
        self.global_test = copy.deepcopy(self.global_res)  # do it after setup_global_res


    def launch(self):
        self.logger.info("Begining launch on %s emb sizes and %s keras layer", len(self.embedding_sizes), len(self.keras_layers))
        for embedding_size in self.embedding_sizes:
            for keras_layer in self.keras_layers:
                evaluation_result, test_result, self.df_prediction = self.run(embedding_size, keras_layer)
                self.logger.info("----------------------------------")
                self.logger.info("EVALUATION RESULT  %s -- \n %s", self.xlsx_name_path, evaluation_result)
                self.logger.info("----------------------------------")
                self.logger.info("TESTING RESULT %s -- \n %s",self.xlsx_name_path,  test_result)
                self.fill_global_res(embedding_size, evaluation_result, test_result)
                self.save_global_res_in_json_file()
                self.fill_row_dfs_in_existing_worksheet(embedding_size, keras_layer)
                self.clean_data()

    def run(self, embedding_size, keras_layer):
        detector_env = self.setup_detector_env(embedding_size, keras_layer,**self.store_env)
        self.XSSConcatenationDetector.setup(**detector_env)

    def fill_global_res(self, embedding_size, result, result_test) :
        for df in self.global_res:
            self.global_res[df][embedding_size].update(result[df][embedding_size])
        if self.testing_model_predicate:
            for df in self.global_test:
                self.global_test[df][embedding_size].update(result_test[df][embedding_size])

    def save_global_res_in_json_file(self):
        ResultDataframeFilesHelper.save_in_json_file(self.json_name_path, self.global_res)
        if self.testing_model_predicate:
            self.logger.debug("TESTING RESULT SAVE? %s", self.testing_model_predicate)
            ResultDataframeFilesHelper.save_in_json_file(self.json_name_path_test, self.global_res)

    def fill_row_dfs_in_existing_worksheet(self, embedding_size, keras_layer=None):
        ResultDataframeFilesHelper.fill_row_dfs_in_existing_worksheet(dict_res=self.global_res, list_df=self.list_df, dfs=self.dfs,
                                                              embedding_size=embedding_size, list_keras_layers=self.keras_layers,
                                                              xlsx_path=self.xlsx_name_path, sheet_name=self.sheet_name)
        if self.testing_model_predicate:
            self.logger.debug("TESTING RESULT SAVE? %s", self.testing_model_predicate)
            ResultDataframeFilesHelper.fill_row_dfs_in_existing_worksheet(dict_res=self.global_test, list_df=self.list_df, dfs=self.dfs_test,
                                                                  embedding_size=embedding_size,
                                                                  list_keras_layers=self.keras_layers, xlsx_path=self.xlsx_name_path,
                                                                  sheet_name=self.sheet_name_test)
        if self.testing_prediction_predicate :
            if keras_layer :
                sheet_name_pred = "pred-" + self.short_name + "-" + embedding_size + "emb" + "-" + str(keras_layer)
                sheet_name_cm = "pred_CM-" + self.short_name + "-" + str(embedding_size) + "emb" + "-" + str(keras_layer)
            else :
                sheet_name_pred = "pred-" + self.short_name + "-" + embedding_size + "emb"
                sheet_name_cm = "pred_CM-" + self.short_name + "-" + str(embedding_size) + "emb"
            PredictionHelper.saveDFPredictionForTestingResults(self.df_prediction, self.xlsx_name_path,
                                                               sheet_name=sheet_name_pred)
            df_confusion_matrix, df_metrix, df_total = PredictionHelper.get_df_prediction_analyzer(
                xlsx=self.xlsx_name_path,
                sheet_name=sheet_name_pred,
                model="construction")
            PredictionHelper.save_df_prediction_analyzer(df_confusion_matrix, df_metrix, df_total,
                                                         xlsx=self.xlsx_name_path,
                                                         sheet_name=sheet_name_cm)

    def clean_data(self):
        if self.generator_usage_predicate:
            tmp_word2vec = os.path.join(self.db_version, "generator", 'models/word2vec/')
            tmp_keras = os.path.join(self.db_version,"generator", 'models/keras/')
            tmp_token = os.path.join(self.db_version,"generator", 'data/list_of_tokens')
            tmp_vector = os.path.join(self.db_version,"generator", 'data/vectors/')
        else:
            tmp_word2vec = os.path.join(self.db_version,"no-generator", 'models/word2vec/')
            tmp_keras = os.path.join(self.db_version,"no-generator", 'models/keras/')
            tmp_token = os.path.join(self.db_version,"no-generator", 'data/list_of_tokens')
            tmp_vector = os.path.join(self.db_version,"no-generator", 'data/vectors/')
        if self.remove_tokens_vectors_predicate:
            shutil.rmtree(os.path.join(self.root_path, tmp_vector), True)
        if self.remove_list_tokens_predicate :
            shutil.rmtree(os.path.join(self.root_path, tmp_token), True)
        if self.remove_word2vec_model_predicate :
            shutil.rmtree(os.path.join(self.root_path, tmp_word2vec), True)
        if self.remove_word2vec_model_predicate :
            shutil.rmtree(os.path.join(self.root_path, tmp_keras), True)

    def setup_detector_env(self, embedding_size, keras_layer, **env):
        self.logger.debug(env)
        res = env.copy()
        del res["embedding_sizes"]
        del res["keras_layers"]
        del res["result_dfs_path"]
        del res["sheet_name"]
        del res["sheet_name_test"]
        # del res["xlsx_name_path"]
        db_path = env["db_path"]
        db_name = env["db_name"]
        self.db_version = env["db_version"]
        lang = self.lang
        self.logger.info("selected language %s", lang)
        res["embedding_size"] = embedding_size
        res["keras_layer"] =keras_layer
        res["dataset_path"] = os.path.join(db_path, db_name)
        if self.generator_usage_predicate:
            tmp_word2vec = os.path.join(self.db_version, "generator", 'models/word2vec/')
            tmp_keras = os.path.join(self.db_version, "generator", 'models/keras/')
            tmp_token = os.path.join(self.db_version, "generator", 'data/list_of_tokens')
            tmp_vector = os.path.join(self.db_version, "generator", 'data/vectors/')
            tmp_plot = os.path.join(self.db_version, "generator", 'history_plot')
            tmp_projector = os.path.join(self.db_version,"generator", 'projectors/')
            tmp_emb = os.path.join(self.db_version,"generator", 'embedding_infos/')
            tmp_json = os.path.join(self.db_version,"generator")
            tmp_json_test = os.path.join(self.db_version,"generator")
        else:
            tmp_word2vec = os.path.join(self.db_version, "no-generator", 'models/word2vec/')
            tmp_keras = os.path.join(self.db_version, "no-generator", 'models/keras/')
            tmp_token = os.path.join(self.db_version, "no-generator", 'data/list_of_tokens')
            tmp_vector = os.path.join(self.db_version, "no-generator", 'data/vectors/')
            tmp_plot = os.path.join(self.db_version, "no-generator", 'history_plot')
            tmp_projector = os.path.join(self.db_version,"no-generator", 'projectors/')
            tmp_emb = os.path.join(self.db_version,"no-generator", 'embedding_infos/')
            tmp_json = os.path.join(self.db_version, "no-generator")
            tmp_json_test = os.path.join(self.db_version, "no-generator")
        res["word2vec_path"] = os.path.join(self.root_path, tmp_word2vec)
        res["word2vec_model_name"] = "word2vec_" + res["short_name"] + "__" + "embedding_size-" + embedding_size+ ".model"
        res["word2vec_model_name_path"] = os.path.join(res["word2vec_path"], res["word2vec_model_name"])
        res["keras_model_name"] = "keras_" + res["short_name"] +"__" + "embedding_size-" +embedding_size + '__keras_layer' + keras_layer + '.h5'
        logging.debug(self.json_path)
        logging.debug(tmp_json)
        logging.debug(self.json_name)
        self.json_name_path = os.path.join(self.json_path, tmp_json, self.json_name)
        self.json_name_path_test = os.path.join(self.json_path, tmp_json_test, self.json_name_test)
        if env["save_keras_model_path"] :
            res["save_keras_model_path"] = os.path.join(res["save_keras_model_path"], tmp_keras)
            res["save_keras_model_name_path"] = os.path.join(res["save_keras_model_path"], tmp_keras, res["keras_model_name"])
        else :
            res["save_keras_model_path"] = os.path.join(self.root_path, tmp_keras)
            res["save_keras_model_name_path"] = os.path.join(res["save_keras_model_path"],res["keras_model_name"])
        res["list_of_tokens_test_name"] = lang +"-data__" + "list_of_tokens__" + res["short_name"] + "/"
        res["list_of_tokens_test_path"] =  os.path.join(self.root_path, tmp_token)
        res["list_of_tokens_test_name_path"] =  os.path.join(res["list_of_tokens_test_path"], res["list_of_tokens_test_name"] )

        res["vectors_test_name"] =  lang +"-data__" + "vectors__" + res["short_name"] + "__" + "embedding_size-" + embedding_size + "/"
        res["vectors_test_path"] =   os.path.join(self.root_path, tmp_vector)
        res["vectors_test_name_path"] =   os.path.join(res["vectors_test_path"], res["vectors_test_name"])
        res["embedding_info_json_file_path"] =   os.path.join(self.root_path, tmp_emb)
        res["embedding_info_json_file_name"] =   "information__" + res["short_name"] + "__" + embedding_size + ".json"
        res["embedding_info_json_file_name_path"] =   os.path.join(res["embedding_info_json_file_path"], res["embedding_info_json_file_name"])
        res["plot_history_path"] =   os.path.join(self.root_path, tmp_plot)
        res["plot_history_loss_name_path"] =  os.path.join(res["plot_history_path"],"LOSS__" + "plot__"+ res["short_name"] + "__" + embedding_size +"emb__"+ keras_layer + ".png")
        res["plot_history_acc_name_path"] =  os.path.join(res["plot_history_path"],"ACC__" + "plot__"+ res["short_name"] + "__" + embedding_size +"emb__"+ keras_layer + ".png")
        res["projector_path"] = os.path.join(self.root_path, tmp_projector)
        FolderHelper.create_folder_if_not_exist(res["plot_history_path"])
        FolderHelper.create_folder_if_not_exist(res["dataset_path"])
        FolderHelper.create_folder_if_not_exist(res["word2vec_path"])
        FolderHelper.create_folder_if_not_exist(res["save_keras_model_path"])
        FolderHelper.create_folder_if_not_exist(res["list_of_tokens_test_name_path"])
        FolderHelper.create_folder_if_not_exist(res["vectors_test_name_path"])
        FolderHelper.create_folder_if_not_exist(res["embedding_info_json_file_path"])
        FolderHelper.create_folder_if_not_exist(res["projector_path"])
        return res


class PreprocessingWorker(multiprocessing.Process):
    def __init__(self, queue, idx, detector, testing_model_predicate):
        super(multiprocessing.Process, self).__init__()
        self.queue = queue
        self.idx = idx
        self.detector = detector
        self.testing_model_predicate = testing_model_predicate

    def run(self) -> None:
        logging.info("Child process start running --> %s and pid %s (parent pid %s)", self.name, self.pid, os.getppid())
        res_validation, res_test, df_prediction =  self.detector.run()
        if self.testing_model_predicate :
            self.queue.put(res_validation)
            self.queue.put(res_test)
            self.queue.put(df_prediction)
            self.queue.put(None)
        else :
            self.queue.put(res_validation)
            self.queue.put(None)
            self.queue.put(None)
            self.queue.put(None)


class PreprocessingLauncher(GenericLauncher):
    def __init__(self,config, **kargs):
        super().__init__(config,**kargs)

    def run(self, embedding_size, keras_layer):
        super().run(embedding_size, keras_layer)
        self.logger.warning("BEGINING OF THE MODEL for %s embedding size and %s layer", embedding_size, keras_layer)
        q = Queue()
        p = PreprocessingWorker(queue=q, idx=1, detector=self.XSSConcatenationDetector, testing_model_predicate=self.testing_model_predicate)
        p.start()
        p.join()
        result = q.get()
        self.logger.debug("RESULT RETRIEVED OK --> %s", q.qsize())
        result_test = q.get()
        self.logger.debug("RESULT RETRIEVED OK --> %s", q.qsize())
        df_prediction = q.get()
        self.logger.debug("RESULT RETRIEVED OK --> %s", q.qsize())
        _ = q.get()
        self.logger.debug("RESULT RETRIEVED OK --> %s", q.qsize())
        p.terminate()
        self.logger.debug("FINISH XSS DETECTOR WORKER")
        return result, result_test, df_prediction


class DefaultLauncher(GenericLauncher):
    def __init__(self, config):
        super().__init__(config)


    def run(self, embedding_size, keras_layer):
        super(DefaultLauncher, self).run(embedding_size, keras_layer)
        evaluation_result, test_result, df_prediction = self.XSSConcatenationDetector.run()
        return evaluation_result, test_result,  df_prediction