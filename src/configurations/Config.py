import os

import yaml
import re
import datetime

from src.helpers.FolderHelper import FolderHelper
from src.helpers.LoggingHelper import LoggingHelper


class Config(LoggingHelper) :

    def __init__(self, conf_env_path_name, conf_ml_path_name):
        super().__init__()
        loader = yaml.FullLoader
        loader.add_implicit_resolver(
            u'tag:yaml.org,2002:float',
            re.compile(u'''^(?:
                     [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
                    |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
                    |\\.[0-9_]+(?:[eE][-+][0-9]+)?
                    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
                    |[-+]?\\.(?:inf|Inf|INF)
                    |\\.(?:nan|NaN|NAN))$''', re.X),
            list(u'-+0123456789.'))

        self.now = str(datetime.datetime.now().strftime("%d-%m-%Y-%Hh-%Mmin-%Ssec"))
        self.yaml_env = yaml.load(open(conf_env_path_name), Loader=loader)
        self.yaml_config = yaml.load(open(conf_ml_path_name), Loader=yaml.FullLoader)
        self.conf_ml_path_name = conf_ml_path_name

        self.global_env = self.yaml_env["global"]
        for k, v in self.yaml_env[Config.__name__].items():
            self.__setattr__(k, v)
        self.logger.setLevel(self.debug_level)
        self.current_env = self.__setup_env()
        self.current_dbs = self.setup_dbs()

    def get_current_lang(self):
        keys = list(self.current_dbs.keys())
        # length = len(keys)
        # if length > 1 :
        #     self.logger.error("We have an issues with the setup of the configuration, we have to have 1 keys instead of %s", length)
        return keys[0]


    def __setup_env(self):
        short_version = self.__getattribute__("db_version")[:2]
        current_env_config = self.yaml_config[self.env]
        res = current_env_config.copy()
        for k in self.env :
            if k.__contains__("path") :
                if k == "save_keras_model_path" or k == "root_path" :
                    res[k] = os.path.join(current_env_config[k], "data_" + short_version)
                else :
                    res[k] = os.path.join(current_env_config[k], "db-" + short_version)
        generator_usage_predicate = self.yaml_env["Manager"]["generator_usage_predicate"]
        if generator_usage_predicate:
            tmp = os.path.join(current_env_config["result_dfs_path"], "db-" + short_version, "generator")
        else:
            tmp = os.path.join(current_env_config["result_dfs_path"], "db-" + short_version, "no-generator")
        FolderHelper.create_folder_if_not_exist(tmp)
        res["xlsx_name_path"] = os.path.join(tmp,  current_env_config["xlsx_name"] + "_" + current_env_config["comments_xlsx_name"] + "_" + self.now + ".xlsx") #FIXED

        res.update(self.global_env)
        return res

    def setup_dbs(self, test_best_models=False):
        mix_dbs = self.yaml_env["db"].copy()
        for lang in self.yaml_env["db"] :
            self.logger.debug("the current lang %s", lang)
            try :
                # db_config = self.yaml_config["db"][lang][self.db_version]
                dbs_config = self.yaml_config["db"][lang][self.db_version]
                self.logger.debug("the current config %s", dbs_config)
            except KeyError as e :
                self.logger.error(e)
                self.logger.error("We can not launch the detector for the  %s version for the %s language because it is"
                                  " not defined in the %s yaml",self.db_version, lang,self.conf_ml_path_name )
                continue
            path_result_dfs = self.current_env["result_dfs_path"]
            ml_data_path = self.current_env["root_path"]
            comments = self.now
            comments_test = self.yaml_config[self.env]["comments_json_test"] + "__" + self.now

            for k in  mix_dbs[lang] :
                self.logger.debug("number dataset %s", k)
                try :
                    self.logger.debug("current dataset that we would like update %s with %s",  mix_dbs[lang][k], dbs_config[k])
                    mix_dbs[lang][k].update(dbs_config[k])
                except KeyError as e :
                    self.logger.error(e)
                    self.logger.error(
                        "We can not launch the detector for the dataset %s with  %s version for the %s language because it is"
                        " not defined in the %s yaml",k, self.db_version, lang, self.conf_ml_path_name)
                    continue
                mix_dbs[lang][k].update( {"db_path": os.path.join(self.yaml_config[self.env]["root_dbs_path"],dbs_config["path"])})
                mix_dbs[lang][k].update( {"ml_data_path": ml_data_path})
                mix_dbs[lang][k].update( {"json_path": path_result_dfs})
                mix_dbs[lang][k].update( {"json_name": lang + "_" + k + "__"+ comments + ".json"})
                mix_dbs[lang][k].update( {"json_name_test": lang + "_" + k  + "__"+ comments_test + ".json"})
                mix_dbs[lang][k].update( {"short_name": lang + "_" + k})
                mix_dbs[lang][k].update( {"xlsx_name_path": self.current_env["xlsx_name_path"]})

            if test_best_models :
                dbs_version = self.yaml_config["db"][lang][self.db_version]
                self.logger.debug(dbs_version)
                for k in mix_dbs[lang] :
                    self.logger.debug(k)
                    tmp =  dbs_version[k].keys()
                    if not "best_embedding" in tmp or not "best_layer" in tmp :
                        raise Exception("You have to add the best_embedding / best_layer configuration values")
                    if not dbs_version[k]["best_embedding"] or not dbs_version[k]["best_layer"] :
                        raise Exception("You have to fill the best_embedding / best_layer configuration values")
                    self.logger.debug(type( mix_dbs[lang][k]["embedding_sizes"]))
                    mix_dbs[lang][k]["embedding_sizes"] = dbs_version[k]["best_embedding"]
                    mix_dbs[lang][k]["keras_layers"] = dbs_version[k]["best_layer"]
                    mix_dbs[lang][k]["sheet_name"] = dbs_version[k]["sheet_name_best_model"]
                    mix_dbs[lang][k]["sheet_name_test"] = dbs_version[k]["sheet_name_best_model_test"]
                    del mix_dbs[lang][k]["best_embedding"]
                    del mix_dbs[lang][k]["best_layer"]
                    del mix_dbs[lang][k]["sheet_name_best_model"]
                    del mix_dbs[lang][k]["sheet_name_best_model_test"]
            else :
                for k in mix_dbs[lang]:
                    del mix_dbs[lang][k]["best_embedding"]
                    del mix_dbs[lang][k]["best_layer"]
                    del mix_dbs[lang][k]["sheet_name_best_model"]
                    del mix_dbs[lang][k]["sheet_name_best_model_test"]
            self.logger.debug("SETUP DB %s", mix_dbs)
        #     print(mix_dbs)
        return mix_dbs

    def __str__(self):
        return str(self.current_env)

    def get_env(self):
        return self.current_env

    def get_db_version(self):
        return self.global_env["db_version"]

    def get_debug_level(self):
        return self.__getattribute__("debug_level")

    def get_dbs(self):
        return self.current_dbs

    def get_global_env(self):
        return self.global_env

    def get_params_of(self, classname):
        params = self.yaml_env["global"].copy()
        params.update(self.yaml_env[classname])
        return params
