import copy
import glob
import json
import os
import time
from pathlib import Path
import re

import tensorflow as tf

import numpy as np
from sklearn.metrics import confusion_matrix
from src.configurations.dfs_name import *
from src.generators import SentencesGenerator
from src.generators.DataGenerator import DataGenerator
from src.helpers.FolderHelper import FolderHelper
from src.helpers.KerasModelHelper import KerasModelHelper
from src.helpers.PartitionHelper import PartitionHelper
from src.generators.SentencesGenerator import SentencesBinariesWithSaveAndRegexGenerator
from src.helpers.LineCodeHelper import LineCodeHelper
from src.helpers.LoggingHelper import LoggingHelper
from src.helpers.PredictionHelper import PredictionHelper
from src.helpers.TokenizationDatasetHelper import TokenizationDatasetHelper
from src.helpers.VectorizationHelper import VectorizationHelper
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper
from src.managers.Word2vecManager import Word2vecManager

class GenericDetector(LoggingHelper):
    def __init__(self, config):
        LoggingHelper.__init__(self)
        self.config = config
        for k, v in self.config.get_params_of(GenericDetector.__name__).items():
            self.__setattr__(k, v)
        self.settup_logging(GenericDetector.__name__)
        self.word2vec_token_model = None
        self.sentences_generator = None
        self.validation_results = {DF_LOSS_TRAINING: {},
                                   DF_ACCURACY_TRAINING: {},
                                   DF_LOSS_EVALUATION: {},
                                   DF_ACCURACY_EVALUATION: {},
                                   DF_PRECISION_EVALUATION: {},
                                   DF_RECALL_EVALUATION: {},
                                   DF_F_MEASURED_EVALUATION: {},
                                   DF_TN_EVALUATION: {},
                                   DF_TP_EVALUATION: {},
                                   DF_FN_EVALUATION: {},
                                   DF_FP_EVALUATION: {},
                                   DF_WORD2VEC_TIME: {},
                                   DF_PADDING_TIME: {},
                                   DF_PARTITION_TIME: {},
                                   DF_KERAS_TRAINING_TIME: {},
                                   DF_KERAS_EVALUATION_TIME: {},
                                   DF_GLOBAL_TIME: {}
                                   }
        self.test_results = None
        self.history = None
        self.validation_test_scores = None
        self.keras_model = None
        self.sentencesGenerator = None
        self.dict_dataset_names = dict()
        self.df_confusion_matrix_prediction = None
        self.df_metrix_prediction = None
        self.df_total_prediction = None
        self.df_prediction = None

class XSSConcatenationDetector(GenericDetector):

    def __init__(self, config):
        GenericDetector.__init__(self, config)
        # self.x_validation, self.y_validation = None
        # self.x_training, self.y_training = None
        # self.x_testing, self.y_testing = None


    def setup(self, **kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)

        for df in self.validation_results:
            self.validation_results[df][self.embedding_size] = {}
        if self.testing_model_predicate:
            self.test_results = copy.deepcopy(self.validation_results)
        self.int_embedding_size = int(self.embedding_size)
        list_path_dataset = os.path.join(self.dataset_path, '*')
        self.logger.debug("What we have to parse %s", list_path_dataset)
        list_dataset = glob.glob(list_path_dataset, recursive=False)
        self.logger.debug("the list of train + test + valid set : %s", list_dataset)
        dataset_names = [Path(list_dataset[0]).parts[-1], Path(list_dataset[1]).parts[-1],
                         Path(list_dataset[2]).parts[-1]]
        dict_name = {"training": None, "testing": None, "validation": None}
        for i in dataset_names:
            if "train" in i:
                dict_name["training"] = i
            elif "test" in i:
                dict_name["testing"] = i
            elif "valid" in i:
                dict_name["validation"] = i
        self.logger.debug("dataset names retrieved %s", dict_name)
        self.dict_dataset_names = dict_name
        if hasattr(self, "extension") :
            self.extension = re.compile(self.extension)

    def run(self):
        step_num = 1
        #If vectors already exist we can skip this step
        start_time_total = time.time()
        self.logger.info("STEP %s - %s - %s emb - %s layer SENTENCES WITH SAVE PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.check_vectors_existence(self.multifiles_predicate)
        self.run_sentences_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer SENTENCES WITH SAVE DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer WORD2VEC MODEL PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_tokenization_process()
        self.run_projector_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer WORD2VEC + SAVE LIST OF TOKEN IN NINARIES FILES DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        start_time_word2vec_end = time.time()

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer PADDING VECTOR  - FILES SAFE/ UNSAFE TRANSLATE INTO VECTORS PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_padding_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer PADDING VECTOR  - FILES SAFE/ UNSAFE TRANSLATE INTO VECTORS PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        start_time_padding_end = time.time()

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer  PARTITION + LABEL PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_partition_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer PARTITION + LABEL PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        start_time_partition_end = time.time()

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer DATA GENERATION PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_data_generation_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer DATA GENERATION PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer CREATION KERAS PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_keras_creation_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer CREATION KERAS PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer FIT KERAS PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_keras_fitting_process()
        self.run_plot_fitting_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer FIT KERAS PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        start_time_keras_creation_end = time.time()

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer EVALUATE KERAS PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        self.run_keras_evaluation_process()
        self.logger.debug("TEST SCORE %s", self.validation_test_scores)
        self.logger.info("STEP %s - %s - %s emb - %s layer EVALUATE KERAS PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        start_time_keras_evaluate_end = time.time()

        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer MEASUREMENT PROCESSING", step_num, self.short_name, self.embedding_size, self.keras_layer)
        confusion_matrix_dict = self.run_measurement_process()
        self.logger.info("STEP %s - %s - %s emb - %s layer MEASUREMENT PROCESSING DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        times_dict = self.run_time_measurement_process(start_time_total, start_time_word2vec_end, start_time_padding_end,
                                                       start_time_partition_end,  start_time_keras_creation_end,
                                                       start_time_keras_evaluate_end)
        self.validation_results.update(confusion_matrix_dict)
        self.validation_results.update(times_dict)
        #if test step predicate
        step_num += 1
        self.logger.info("STEP %s - %s - %s emb - %s layer TESTING KERAS", step_num, self.short_name, self.embedding_size, self.keras_layer)
        if self.testing_model_predicate :
            self.run_keras_testing_process()
            confusion_matrix_dict = self.run_measurement_process(self.testing_model_predicate)
            times_dict = self.run_time_measurement_process(None, None, None, None, None, None)
            self.test_results.update(confusion_matrix_dict)
            self.test_results.update(times_dict)
            if self.testing_prediction_predicate :
                self.run_testing_prediction_process()
            else :
                 self.df_prediction = None

        else :
            self.test_results = None
        self.logger.info("STEP %s - %s - %s emb - %s layer  TESTING KERAS DONE", step_num, self.short_name, self.embedding_size, self.keras_layer)
        return self.validation_results, self.test_results, self.df_prediction

    def get_y_validation_prediction_values(self):
        if not hasattr(self, 'keras_model') or not self.keras_model:
            self.logger.error(str(self.__class__) + "has no keras model attribute -- you have to train the model first")
            return []
        Y_pred = self.keras_model.predict(self.x_validation)
        y_pred = np.around(Y_pred).astype(int)
        return y_pred

    def get_y_testing_prediction_values(self):
        if not hasattr(self, 'keras_model') or not self.keras_model:
            self.logger.error(str(self.__class__) + "has no keras model attribute -- you have to train the model first")
            return []
        Y_pred = self.keras_model.predict(self.x_testing)
        y_pred = np.around(Y_pred).astype(int)
        return y_pred

    def get_y_testing_values(self):
        if not hasattr(self, 'y_testing') or not len(self.y_testing):
            self.logger.error(str(self.__class__) + "has no y_testing attribute")
            return []
        return self.y_testing

    def get_y_validation_values(self):
        if not hasattr(self, 'y_validation') or not len(self.y_validation):
            self.logger.error(str(self.__class__) + "has no y_validation attribute")
            return []
        return self.y_validation

    def run_measurement_process(self,testing_model_predicate=False):
        if testing_model_predicate :
            train_acc = None
            train_loss = None
            eval_acc = self.get_testing_accuracy()
            eval_loss = self.get_testing_loss()
            y = self.get_y_testing_values()
            y_pred = self.get_y_testing_prediction_values()
        else :
            train_acc = self.get_training_accuracy()
            train_loss = self.get_training_loss()
            eval_acc = self.get_validation_accuracy()
            eval_loss = self.get_validation_loss()
            y = self.get_y_validation_values()
            y_pred = self.get_y_validation_prediction_values()
        tn, fp, fn, tp = confusion_matrix(y, y_pred).ravel()if len(y_pred) > 0 and len(y) > 0 else (0,0,0,0)
        self.logger.warning("tn=%s , fp=%s, fn=%s, tp=%s", tn, fp, fn, tp)
        try:
            if tp == 0 and fp == 0 :
                precision = 0.0
            else :
                precision = tp / (tp + fp)  # 99.4%
        except Exception as e:
            self.logger.warning(e)
            precision = 0.0
        self.logger.warning("Precision score : %f", precision)
        # The recall is intuitively the ability of the classifier to find all the positive samples
        try:
            if tp == 0 and fn == 0 :
                recall = 0
            else :
                recall = tp / (tp + fn)  # 98.8%
        except Exception as e:
            self.logger.warning(e)
            recall = 0.0

        self.logger.warning("Recall score : %f", recall)
        # The F1 score can be interpreted as a weighted average of the precision and recall, where an F1 score reaches its best value at 1 and worst score at 0.
        try:
            if precision == 0 and recall == 0 :
                F1 = 0
            else :
                F1 = 2 * (precision * recall) / (precision + recall)  # 0.99 ==> 99%
        except Exception as e:
            self.logger.warning(e)
            F1 = 0
        self.logger.warning("f-measure score : %f", F1)

        return {
            DF_ACCURACY_TRAINING: {self.embedding_size: {self.keras_layer: train_acc}},
            DF_LOSS_TRAINING: {self.embedding_size: {self.keras_layer: train_loss}},
            DF_ACCURACY_EVALUATION: {self.embedding_size: {self.keras_layer: eval_acc}},
            DF_LOSS_EVALUATION: {self.embedding_size: {self.keras_layer: eval_loss}},
            DF_PRECISION_EVALUATION: {self.embedding_size: {self.keras_layer: float(precision)}},
            DF_RECALL_EVALUATION: {self.embedding_size: {self.keras_layer: float(recall)}},
            DF_F_MEASURED_EVALUATION: {self.embedding_size: {self.keras_layer: float(F1)}},
            DF_TN_EVALUATION: {self.embedding_size: {self.keras_layer: int(tn)}},
            DF_TP_EVALUATION: {self.embedding_size: {self.keras_layer: int(tp)}},
            DF_FN_EVALUATION: {self.embedding_size: {self.keras_layer: int(fn)}},
            DF_FP_EVALUATION: {self.embedding_size: {self.keras_layer: int(fp)}}
         }

    def run_time_measurement_process(self,start_time_total, start_time_word2vec_end, start_time_padding_end,
                                                       start_time_partition_end,  start_time_keras_creation_end,
                                                       start_time_keras_evaluate_end):
        if not start_time_total :
            global_time = None
            word2vec_time = None
            padding_time = None
            partition_time = None
            keras_training_time = None
            keras_evaluation_time = None
        else :
            global_time = float(time.time() - start_time_total)
            word2vec_time = float(start_time_word2vec_end - start_time_total)
            padding_time = float(start_time_padding_end - start_time_word2vec_end)
            partition_time = float(start_time_partition_end - start_time_padding_end)
            keras_training_time = float(start_time_keras_creation_end - start_time_partition_end)
            keras_evaluation_time = float(start_time_keras_evaluate_end - start_time_keras_creation_end)
        return {DF_WORD2VEC_TIME : {self.embedding_size : {self.keras_layer : word2vec_time}},
                DF_PADDING_TIME : {self.embedding_size : {self.keras_layer : padding_time}},
                DF_PARTITION_TIME : {self.embedding_size : {self.keras_layer : partition_time}},
                DF_KERAS_TRAINING_TIME : {self.embedding_size : {self.keras_layer : keras_training_time}},
                DF_KERAS_EVALUATION_TIME : {self.embedding_size : {self.keras_layer : keras_evaluation_time}},
                DF_GLOBAL_TIME : {self.embedding_size : {self.keras_layer : global_time}}
                }

    def get_training_accuracy(self):
        if not hasattr(self, 'history') or not self.history :
            self.logger.error(str(self.__class__) + "has no history attribute -- you have to train the model first")
            return 0
        try :
            res =  self.history.history["acc"][-1]
        except KeyError as e :
            self.logger.debug("acc is not the goog key... worked on tensorflow 1.14.0")
            res = self.history.history["accuracy"][-1]
        return float(res)

    def get_training_loss(self):
        if not hasattr(self, 'history') or not self.history :
            self.logger.error(str(self.__class__) + "has no history attribute -- you have to train the model first")
            return 0
        return float(self.history.history["loss"][-1])

    def get_validation_accuracy(self):
        if not hasattr(self, 'validation_test_scores') or not self.validation_test_scores :
            self.logger.error(str(self.__class__) + "has no validation_test_scores attribute -- you have to train the model first")
            return 0
        return float(self.validation_test_scores[1])

    def get_validation_loss(self):
        if not hasattr(self, 'validation_test_scores') or not self.validation_test_scores :
            self.logger.error(str(self.__class__) + "has no validation_test_scores attribute -- you have to train the model first")
            return 0
        return float(self.validation_test_scores[0])

    def get_testing_accuracy(self):
        if not hasattr(self, 'testing_test_scores') or not self.testing_test_scores:
            self.logger.error(
                str(self.__class__) + "has no testing_test_scores attribute -- you have to train the model first")
            return 0
        return float(self.testing_test_scores[1])

    def get_testing_loss(self):
        if not hasattr(self, 'testing_test_scores') or not self.testing_test_scores:
            self.logger.error(
                str(self.__class__) + "has no testing_test_scores attribute -- you have to train the model first")
            return 0
        return float(self.testing_test_scores[0])

    def run_sentences_process(self):
        if not self.vectors_already_exist_predicate :
            # self.logger.debug(self.__dict__)
            # pprint(self.__dict__)
            self.logger.debug("dirname %s",self.dataset_path)
            self.logger.debug("savePath %s ",self.list_of_tokens_test_name_path)
            self.logger.debug(self.save_list_tokens_predicate)
            if not self.vectors_already_exist_predicate :
                res = TokenizationDatasetHelper.createListTokensFor(dirname=self.dataset_path, savePath=self.list_of_tokens_test_name_path,
                                          methodToConvertLineToListOfTokens=LineCodeHelper.convertLineOfLongByteToToken,
                                          save_predicate=self.save_list_tokens_predicate, multifiles_predicate=self.multifiles_predicate)
                if isinstance(res, bool) and res :
                    ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE=True #TODO Have to use it in this detector
                else :
                    ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE = False
                self.sentences_generator = SentencesGenerator.SentencesWithSave(dirname=self.dataset_path,
                                                                                savePath=self.list_of_tokens_test_name_path)  #TODO Have to change
                # self.sentences_generator = SentencesBinariesWithSaveAndRegexGenerator(dirname=self.dataset_path,
                #                                                                       savePath=self.list_of_tokens_test_name_path,
                #                                                                       methodToConvertLineToListOfTokens=LineCodeHelper.convertLineByWord,
                #                                                                       save_predicate=self.save_list_tokens_predicate,
                #                                                                       already_save_predicate=ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE)

    def run_tokenization_process(self):
        if not self.vectors_already_exist_predicate:
            param = self.word2vec_parameters
            args = {
                "model_name" : self.word2vec_model_name ,
                "model_path": self.word2vec_path,
                "size": self.int_embedding_size,
                "sentences_generator" : self.sentences_generator,
                "save_word2vec_model_predicate" : self.save_word2vec_model_predicate
            }
            param.update(args)
            self.logger.debug("ARGS TO WORD2VEC %s", args)
            self.word2vec_token_model = Word2vecManager.getWord2VecModel(**param)


    def run_padding_process(self):
        TRAIN_DATASET = os.path.join(self.dataset_path, self.dict_dataset_names["training"])
        VALID_DATASET = os.path.join(self.dataset_path,  self.dict_dataset_names["validation"])
        TEST_DATASET = os.path.join(self.dataset_path, self.dict_dataset_names["testing"])
        self.logger.debug("HEEEEERREEEE")
        self.logger.debug(TRAIN_DATASET)
        self.logger.debug(VALID_DATASET)
        self.logger.debug(TEST_DATASET)
        X_train_NameFileList, Y_train_list, train_informationsFiles = VectorizationHelper.get_XY_lists_from_dataset(dataset=TRAIN_DATASET)
        X_valid_NameFileList, Y_valid_list, valid_informationsFiles = VectorizationHelper.get_XY_lists_from_dataset(dataset=VALID_DATASET)
        X_test_NameFileList, Y_test_list, test_informationsFiles = VectorizationHelper.get_XY_lists_from_dataset(dataset=TEST_DATASET)
        self.logger.debug("Showing the correspondance between the suffled lists")
        self.logger.debug("X_NameFileList[0] -> %s -- unsafe? %s", X_train_NameFileList[0], Y_train_list[0])
        self.logger.debug("X_NameFileList[1] -> %s -- unsafe? %s", X_train_NameFileList[1], Y_train_list[1])
        self.logger.debug("X_NameFileList[2] -> %s -- unsafe? %s", X_train_NameFileList[2], Y_train_list[2])
        self.logger.debug("Getting padding vectors representation of each of the %d files", len(X_train_NameFileList))
        train_vectors, train_number_of_tokens, _ = VectorizationHelper.getLists(X_train_NameFileList, self.word2vec_token_model)
        valid_vectors, valid_number_of_tokens, _ = VectorizationHelper.getLists(X_valid_NameFileList,  self.word2vec_token_model)
        test_vectors, test_number_of_tokens, _ = VectorizationHelper.getLists(X_test_NameFileList,  self.word2vec_token_model)
        train_max_number_of_tokens = max(train_number_of_tokens)
        valid_max_number_of_tokens = max(valid_number_of_tokens)
        test_max_number_of_tokens = max(test_number_of_tokens)
        self.logger.info("MAX SIZE VECTOR TRAIN ----> %d ", train_max_number_of_tokens)
        self.logger.info("MAX SIZE VECTOR VALID ----> %d ", valid_max_number_of_tokens)
        self.logger.info("MAX SIZE VECTOR TEST ----> %d ", test_max_number_of_tokens)
        MAX_NUMBER_OF_TOKEN = max(train_max_number_of_tokens, valid_max_number_of_tokens, test_max_number_of_tokens)
        self.max_size_vector = MAX_NUMBER_OF_TOKEN * int(self.embedding_size)
        self.logger.info("size maximun for a vector representing a file --> %d", self.max_size_vector)
        train_vectors_padding = [np.pad(vector, (0, (self.max_size_vector - len(vector))), constant_values=0) for vector in
                                 train_vectors]
        valid_vectors_padding = [np.pad(vector, (0, (self.max_size_vector - len(vector))), constant_values=0) for vector in
                                 valid_vectors]
        test_vectors_padding = [np.pad(vector, (0, (self.max_size_vector - len(vector))), constant_values=0) for vector in
                                test_vectors]
        self.logger.info("training vector shape %s", train_vectors_padding[0].shape)
        self.logger.info("valid vector shape %s", valid_vectors_padding[0].shape)
        self.logger.info("test vector shape %s", test_vectors_padding[0].shape)
        self.x_training, self.y_training, self.x_validation, self.y_validation = np.asarray(train_vectors_padding), np.transpose(
            np.expand_dims(np.asarray(Y_train_list), axis=0)), np.asarray(valid_vectors_padding), np.transpose(
            np.expand_dims(np.asarray(Y_valid_list), axis=0))
        if self.testing_model_predicate :
            self.x_testing, self.y_testing = np.asarray(test_vectors_padding), np.transpose(
                np.expand_dims(np.asarray(Y_test_list), axis=0))
        information = {}
        information["MAX_SIZE_VECTOR"] = self.max_size_vector
        information["TOKEN_SIZE"] = self.int_embedding_size
        information["MAX_NUMBER_TOKEN"] = int(self.max_size_vector / self.int_embedding_size)

        ResultDataframeFilesHelper.save_in_json_file(self.embedding_info_json_file_name_path, information)
        self.logger.info("MAX SIZE VECTOR ----> %d ", self.max_size_vector)

    def run_partition_process(self):
        pass

    def run_data_generation_process(self):
        pass

    def run_keras_creation_process(self):
        self.keras_model = KerasModelHelper.getKerasModel(self.x_training,
                                                                      model_name=self.keras_model_name,
                                                                      path=self.save_keras_model_path,
                                                                      keras_layers=self.keras_layer,
                                                                      save_keras_predicate=self.save_keras_predicate)

    def run_keras_fitting_process(self):
        callbacks_list = [
            # tf.keras.callbacks.ReduceLROnPlateau(
            #     monitor='val_loss',
            #     factor=0.1,
            #     patience=10,
            #     verbose=1
            # ),
            # tf.keras.callbacks.EarlyStopping(
            #     monitor='accuracy',
            #     patience=10,
            #     verbose=1
            # ),
            # tf.keras.callbacks.EarlyStopping(
            #     monitor='val_recall',
            #     patience=20,
            #     mode='max',
            #     restore_best_weights=True,
            #     verbose=1
            # ),
            # tf.keras.callbacks.EarlyStopping(
            #     monitor='val_accuracy',
            #     patience=20,
            #     mode='max',
            #     restore_best_weights=True,
            #     verbose=1
            # ),
            # tf.keras.callbacks.ModelCheckpoint(
            #     filepath=os.path.join(SAVE_KERAS_MODEL, MODEL_KERAS_NAME),
            #     monitor='recall',
            #     save_best_only=True,
            # ),
            # tf.keras.callbacks.ModelCheckpoint(
            #     filepath=os.path.join(SAVE_KERAS_MODEL, MODEL_KERAS_NAME),
            #     monitor='val_loss',
            #     save_best_only=True,
            # ),
        ]
        if self.tensorbord_analysis_predicate:
            callbacks_list.append(
                tf.keras.callbacks.TensorBoard(log_dir=self.tensorboard_log_path, histogram_freq=1, embeddings_freq=1))
        if self.tensorbord_model_checkpoint_predicate:
            callbacks_list.append(
                tf.keras.callbacks.ModelCheckpoint(
                    filepath=os.path.join(self.keras_model_path, self.keras_model_name),
                    # save_weights_only=True, #can not load the model after ...
                    save_weights_only=False,
                    monitor='recall',
                    mode='max',
                    save_best_only=True,
                ))
        self.history = self.keras_model.fit(self.x_training, self.y_training, epochs=self.keras_parameters["fit_epochs"],
                                            callbacks=callbacks_list,
                                            validation_data=(self.x_validation, self.y_validation), verbose=2)

    def run_plot_fitting_process(self):
        acc = self.history.history['accuracy']
        val_acc = self.history.history['val_accuracy']
        loss = self.history.history['loss']
        val_loss = self.history.history['val_loss']
        epochs = range(len(acc))
        import matplotlib.pyplot as plt
        myFig = plt.figure()
        plt.plot(epochs, acc, 'bo', label='Training accuracy')
        plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
        plt.title('Training and validation accuracy')
        plt.legend()

        myFig.savefig(self.plot_history_acc_name_path, format="png")
        plt.close(myFig) #We have to close manually the plot because it's still open when we created it

        myFig = plt.figure()
        plt.plot(epochs, loss, 'bo', label='Training Loss')
        plt.plot(epochs, val_loss, 'b', label='Validation Loss')
        plt.title('Training and validation loss')
        plt.legend()
        myFig.savefig(self.plot_history_loss_name_path, format="png")
        plt.close(myFig)

    def run_keras_evaluation_process(self):
        self.validation_test_scores = self.keras_model.evaluate(self.x_validation, self.y_validation, verbose=2)

    def run_keras_testing_process(self):
        self.testing_test_scores = self.keras_model.evaluate(self.x_testing, self.y_testing, verbose=2)

    def run_projector_process(self):
        if hasattr(self, 'save_projector_word2vec_predicate') and self.save_projector_word2vec_predicate:
            import io
            import os
            # Vector file, `\t` seperated the vectors and `\n` seperate the words
            # https://gist.github.com/BrikerMan/7bd4e4bd0a00ac9076986148afc06507
            """
            0.1\t0.2\t0.5\t0.9
            0.2\t0.1\t5.0\t0.2
            0.4\t0.1\t7.0\t0.8
            """
            out_v = io.open(
                os.path.join(self.projector_path, self.word2vec_model_name + "__" + 'vecs.tsv'), 'w',
                encoding='utf-8')

            # Meta data file, `\n` seperated word
            """
            token1
            token2
            token3
            """
            out_m = io.open(
                os.path.join(self.projector_path, self.word2vec_model_name + "__" + 'meta.tsv'), 'w',
                encoding='utf-8')

            # Write meta file and vector file
            for index in range(len(self.word2vec_token_model.wv.index_to_key)):
                word = self.word2vec_token_model.wv.index_to_key[index]
                vec = self.word2vec_token_model.wv.vectors[index]
                out_m.write(word + "\n")
                out_v.write('\t'.join([str(x) for x in vec]) + "\n")
            out_v.close()
            out_m.close()

    def check_vectors_existence(self, multifiles_predicate=False):
        if multifiles_predicate :
            list_dataset = FolderHelper.get_list_dir_recursively(self.dataset_path, format="str")
        else :
            list_dataset = glob.glob(os.path.join(self.dataset_path, '*', ''), recursive=False)
        len_list_dataset = len(list_dataset)
        self.logger.debug(list_dataset)
        list_vector_dataset = glob.glob(os.path.join(self.vectors_test_name_path, '*', ''), recursive=False)
        len_list_vector_dataset = len(list_vector_dataset)
        self.logger.debug("len_list_dataset %s with len_list_vector_dataset= %s",
                          len_list_dataset, len_list_vector_dataset)
        if len_list_vector_dataset == 0:
            self.vectors_already_exist_predicate = False
            self.logger.debug("vectors_already_exist_predicate %s with len_list_vector_dataset= %s",
                              self.vectors_already_exist_predicate, len_list_vector_dataset)
        elif len_list_dataset > 0 and len_list_dataset == len_list_vector_dataset:
            list_vector_file_0 = len(glob.glob(os.path.join(list_vector_dataset[0], "*", "*.*"), recursive=True))
            list_vector_file_1 = len(glob.glob(os.path.join(list_vector_dataset[1], "*", "*.*"), recursive=True))
            list_vector_file_2 = len(glob.glob(os.path.join(list_vector_dataset[2], "*", "*.*"), recursive=True))
            list_file_0 = len(glob.glob(os.path.join(list_dataset[0], "*", "*.*"), recursive=True))
            list_file_1 = len(glob.glob(os.path.join(list_dataset[1], "*", "*.*"), recursive=True))
            list_file_2 = len(glob.glob(os.path.join(list_dataset[2], "*", "*.*"), recursive=True))
            dict_res = {
                "train": list_file_0,
                "test": list_file_1,
                "valid": list_file_2,
                "train_vect": list_vector_file_0,
                "test_vect": list_vector_file_1,
                "valid_vect": list_vector_file_2,
            }
            if list_file_0 == 0 or list_file_1 == 0 or list_file_2 == 0:
                self.vectors_already_exist_predicate = False
                self.logger.error(
                    "Issue with the dataset -- verify the path the database %s",
                    self.dataset_path)
            else:
                self.logger.debug("vectors_already_exist_predicate %s", dict_res)
                if list_vector_file_0 != list_file_0 or list_vector_file_1 != list_file_1 or list_vector_file_2 != list_file_2:
                    self.vectors_already_exist_predicate = False
                    self.logger.error(
                        "Issue with one of the dataset vector -- have to rebuild all -- vectors_already_exist_predicate %s",
                        self.vectors_already_exist_predicate)
                elif list_vector_file_0 == list_file_0 or list_vector_file_1 == list_file_1 or list_vector_file_2 == list_file_2:
                    self.vectors_already_exist_predicate = True
        else:
            self.vectors_already_exist_predicate = False
        self.logger.debug("vectors_already_exist_predicate %s", self.vectors_already_exist_predicate)


    def run_testing_prediction_process(self):
        root_dataset_path = str(os.path.join(self.dataset_path, self.dict_dataset_names["testing"]))
        self.logger.debug(root_dataset_path)
        self.df_prediction = PredictionHelper.getPredictionRegardingRules(root_dataset_path, self.keras_model, self.x_testing,
                                                    self.prediction_name, self.y_testing)
        self.logger.warning("Len of dict prediction --> %s", len(self.df_prediction))


class XSSGeneratorConcatenationDetector(XSSConcatenationDetector):
    def __init__(self, config):
        XSSConcatenationDetector.__init__(self, config)
        self.partition = None
        self.labels = None
        self.information = None

    def get_y_validation_prediction_values(self):
        if not hasattr(self, 'keras_model') or not self.keras_model:
            self.logger.error(str(self.__class__) + "has no keras_model attribute -- you have to train the model first")
            return []
        Y_pred = self.keras_model.predict(self.validation_generator)
        y_pred = np.around(Y_pred).astype(int)
        return y_pred

    def get_y_testing_prediction_values(self):
        if not hasattr(self, 'testing_generator') or not self.testing_generator:
            self.logger.error(str(self.__class__) + "has no testing_generator attribute -- you have to generate it first")
            return []
        Y_pred = self.keras_model.predict(self.testing_generator)
        y_pred = np.around(Y_pred).astype(int)
        return y_pred

    def get_y_testing_values(self):
        if not hasattr(self, 'testing_generator') or not self.testing_generator:
            self.logger.error(str(self.__class__) + "has no testing_generator attribute -- you have to generate it first")
            return []
        return np.concatenate([self.testing_generator[i][1] for i in range(len(self.testing_generator))])

    def get_y_validation_values(self):
        if not hasattr(self, 'validation_generator') or not self.validation_generator:
            self.logger.error(str(self.__class__) + "has no validation_generator attribute -- you have to generate it first")
            return []
        return np.concatenate([self.validation_generator[i][1] for i in range(len(self.validation_generator))])

    def run_padding_process(self):
        self.logger.debug("vectors_already_exist_predicate %s", self.vectors_already_exist_predicate)
        if not self.vectors_already_exist_predicate:
            self.logger.debug("Have to create the padding vectors")
            self.max_size_vector = VectorizationHelper.createPaddingVectorsFor(self.list_of_tokens_test_name_path,
                                                                               self.vectors_test_name_path,
                                                                               self.word2vec_token_model,
                                                                               self.int_embedding_size, multifiles_predicate=self.multifiles_predicate)
            information = {}
            information["MAX_SIZE_VECTOR"] = self.max_size_vector
            information["TOKEN_SIZE"] = self.int_embedding_size
            information["MAX_NUMBER_TOKEN"] = int(
                self.max_size_vector / self.int_embedding_size) if self.max_size_vector else None

            ResultDataframeFilesHelper.save_in_json_file(self.embedding_info_json_file_name_path, information)
        else:
            self.logger.debug("padding vectors already creating")
            try:
                with open(self.embedding_info_json_file_name_path) as json_file:
                    information = json.load(json_file)
                    self.max_size_vector = information["MAX_SIZE_VECTOR"]
            except FileNotFoundError as e:
                self.vectors_already_exist_predicate = False
                self.run_sentences_process()
                self.run_tokenization_process()
                self.run_projector_process()
                self.max_size_vector = VectorizationHelper.createPaddingVectorsFor(self.list_of_tokens_test_name_path,
                                                                                   self.vectors_test_name_path,
                                                                                   self.word2vec_token_model,
                                                                                   self.int_embedding_size, multifiles_predicate=self.multifiles_predicate)
                information = {}
                information["MAX_SIZE_VECTOR"] = self.max_size_vector
                information["TOKEN_SIZE"] = self.int_embedding_size
                information["MAX_NUMBER_TOKEN"] = int(self.max_size_vector / self.int_embedding_size)

                ResultDataframeFilesHelper.save_in_json_file(self.embedding_info_json_file_name_path, information)
        self.logger.info("MAX SIZE VECTOR ----> %d ", self.max_size_vector)

    def run_sentences_process(self):
        if not self.vectors_already_exist_predicate :
            self.logger.debug(self.__dict__)
            # pprint(self.__dict__)
            self.logger.debug("dirname %s",self.dataset_path)
            self.logger.debug("savePath %s ",self.list_of_tokens_test_name_path)
            self.logger.debug("save_list_tokens_predicate %s ",self.save_list_tokens_predicate)
            self.logger.debug("multifiles_predicate %s ",self.multifiles_predicate)
            if not self.vectors_already_exist_predicate :
                if self.extension :
                    res = TokenizationDatasetHelper.createListTokensFor(dirname=self.dataset_path, savePath=self.list_of_tokens_test_name_path,
                                                                        methodToConvertLineToListOfTokens=getattr(LineCodeHelper, self.how_convert_line_to_tokens),
                                                                        save_predicate=self.save_list_tokens_predicate, multifiles_predicate=self.multifiles_predicate, extension=self.extension)
                else :
                    #Extension fixed by default in the parameter
                    res = TokenizationDatasetHelper.createListTokensFor(dirname=self.dataset_path, savePath=self.list_of_tokens_test_name_path,
                                          methodToConvertLineToListOfTokens=LineCodeHelper.convertLineOfLongByteToToken,
                                          save_predicate=self.save_list_tokens_predicate, multifiles_predicate=self.multifiles_predicate)
                if isinstance(res, bool) and res :
                    ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE=True #TODO Have to put this predicate in config
                else :
                    ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE = False
                if self.extension :
                    self.sentences_generator = SentencesBinariesWithSaveAndRegexGenerator(dirname=self.dataset_path,
                                                                                          savePath=self.list_of_tokens_test_name_path,
                                                                                          methodToConvertLineToListOfTokens=LineCodeHelper.convertLineByWord,
                                                                                          save_predicate=self.save_list_tokens_predicate,
                                                                                          already_save_predicate=ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE,
                                                                                          multifiles_predicate=self.multifiles_predicate,
                                                                                          extension=self.extension)
                else :
                    self.sentences_generator = SentencesBinariesWithSaveAndRegexGenerator(dirname=self.dataset_path,
                                                                                      savePath=self.list_of_tokens_test_name_path,
                                                                                      methodToConvertLineToListOfTokens=LineCodeHelper.convertLineByWord,
                                                                                      save_predicate=self.save_list_tokens_predicate,
                                                                                      already_save_predicate=ALREADY_LIST_OF_LIST_TOKENS_SAFE_PREDICATE,
                                                                                      multifiles_predicate=self.multifiles_predicate)


    def run_partition_process(self):
        self.partition, self.labels, self.information = PartitionHelper.generatePartitionAndLabelsForTheWholeDataset(self.dataset_path, self.multifiles_predicate, self.extension)

    def run_data_generation_process(self):
        # if self.multifiles_predicate :

        params = self.generator_parameters["training"]
        params["dim"] = self.max_size_vector
        self.training_generator = DataGenerator(self.partition[self.dict_dataset_names["training"]], self.labels, self.vectors_test_name_path, **params)
        params = self.generator_parameters["testing"]
        params["dim"] = self.max_size_vector
        self.testing_generator = DataGenerator(self.partition[self.dict_dataset_names["testing"]], self.labels, self.vectors_test_name_path, **params)
        params = self.generator_parameters["validation"]
        params["dim"] = self.max_size_vector
        self.validation_generator = DataGenerator(self.partition[self.dict_dataset_names["validation"]], self.labels, self.vectors_test_name_path, **params)
        self.logger.debug("training generator %s", self.training_generator)
        self.logger.debug("testing generator %s", self.testing_generator)
        self.logger.debug("validation generator %s", self.validation_generator)


    def run_keras_creation_process(self):
        self.keras_model = KerasModelHelper.getKerasModelWithGenerator(self.training_generator, model_name=self.keras_model_name,
                                                     path=self.save_keras_model_path, keras_layers=self.keras_layer, save_keras_predicate=self.save_keras_predicate)


    def run_keras_fitting_process(self):
        callbacks_list = [
            # tf.keras.callbacks.ReduceLROnPlateau(
            #     monitor='val_loss',
            #     factor=0.1,
            #     patience=10,
            #     verbose=1
            # ),
            # tf.keras.callbacks.EarlyStopping(
            #     monitor='accuracy',
            #     patience=10,
            #     verbose=1
            # ),
            # tf.keras.callbacks.EarlyStopping(
            #     monitor='val_recall',
            #     patience=20,
            #     mode='max',
            #     restore_best_weights=True,
            #     verbose=1
            # ),
            # tf.keras.callbacks.EarlyStopping(
            #     monitor='val_accuracy',
            #     patience=20,
            #     mode='max',
            #     restore_best_weights=True,
            #     verbose=1
            # ),
            # tf.keras.callbacks.ModelCheckpoint(
            #     filepath=os.path.join(SAVE_KERAS_MODEL, MODEL_KERAS_NAME),
            #     monitor='recall',
            #     save_best_only=True,
            # ),
            # tf.keras.callbacks.ModelCheckpoint(
            #     filepath=os.path.join(SAVE_KERAS_MODEL, MODEL_KERAS_NAME),
            #     monitor='val_loss',
            #     save_best_only=True,
            # ),
        ]
        if self.tensorbord_analysis_predicate :
            callbacks_list.append(tf.keras.callbacks.TensorBoard(log_dir=self.tensorboard_log_path, histogram_freq=1, embeddings_freq=1))
        if self.tensorbord_model_checkpoint_predicate :
            callbacks_list.append(
                tf.keras.callbacks.ModelCheckpoint(
                    filepath=os.path.join(self.keras_model_path, self.keras_model_name),
                    # save_weights_only=True, #can not load the model after ...
                    save_weights_only=False,
                    monitor='recall',
                    mode='max',
                    save_best_only=True,
                ))
        self.history = self.keras_model.fit(self.training_generator, epochs=self.keras_parameters["fit_epochs"], callbacks=callbacks_list,
                                 validation_data=self.validation_generator, verbose=2)
        #TODO REMOVE AFTER TESTING IT s work
        
        # acc = self.history.history['accuracy']
        # val_acc = self.history.history['val_accuracy']
        # loss = self.history.history['loss']
        # val_loss = self.history.history['val_loss']
        # epochs = range(len(acc))
        # import matplotlib.pyplot as plt
        # myFig = plt.figure()
        # plt.plot(epochs, acc, 'bo', label='Training accuracy')
        # plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
        # plt.title('Training and validation accuracy')
        # plt.legend()
        # 
        # myFig.savefig(self.plot_history_acc_name_path, format="png")
        # 
        # myFig = plt.figure()
        # plt.plot(epochs, loss, 'bo', label='Training Loss')
        # plt.plot(epochs, val_loss, 'b', label='Validation Loss')
        # plt.title('Training and validation loss')
        # plt.legend()
        # myFig.savefig(self.plot_history_loss_name_path, format="png")

    def run_keras_evaluation_process(self):
        self.validation_test_scores = self.keras_model.evaluate(self.validation_generator, verbose=2)

    def run_keras_testing_process(self):
        self.testing_test_scores = self.keras_model.evaluate(self.testing_generator, verbose=2)

    def run_testing_prediction_process(self):
        root_dataset_path = str(os.path.join(self.dataset_path, self.dict_dataset_names["testing"]))
        self.logger.debug(root_dataset_path)
        self.df_prediction = PredictionHelper.getPredictionRegardingRules(root_dataset_path, self.keras_model, self.testing_generator,
                                                    self.prediction_name)
        self.logger.warning("Len of dict prediction --> %s", len(self.df_prediction))