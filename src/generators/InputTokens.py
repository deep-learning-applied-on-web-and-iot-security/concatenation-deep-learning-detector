class InputTokens(object) :
    def __init__(self, filename):
        self.filename = filename.strip()

    def __iter__(self):
        res = []
        for line in open(self.filename, 'r'):
            if len(line) != 1:
                res = res + line.split()
        for token in res :
            yield token