import glob
import logging
import os
import pickle
import re
from pathlib import Path

from src.helpers.FolderHelper import FolderHelper
from src.helpers.LineCodeHelper import LineCodeHelper


from src.helpers.LoggingHelper import StaticLoggingHelper



class SentencesBinariesWithSaveAndRegexGenerator(StaticLoggingHelper):
    logger = logging.getLogger(__name__)

    def __init__(self, dirname, savePath, extension=re.compile(".*(\.php|\.js|\.zip|\.tar\.gz|\.tgz|\.bin)"), methodToConvertLineToListOfTokens=LineCodeHelper.convertLineOfLongByteToToken,
                 save_predicate=True, already_save_predicate=False, multifiles_predicate=False):
        """

        :param dirname:
        :param savePath:
        :param extension:
        :param methodToConvertLineToListOfTokens:
        :param save_predicate:
        :param already_save_predicate:
        """
        self.already_save_predicate = already_save_predicate
        self.savePath = savePath
        self.dirname = dirname
        SentencesBinariesWithSaveAndRegexGenerator.logger.debug("directory name --> %s", dirname)
        if already_save_predicate :
            tmp_path = self.savePath
        else :
            tmp_path = self.dirname
        if multifiles_predicate :
            self.list_dir = FolderHelper.get_list_dir_recursively(tmp_path, format="str")
        else :
            self.list_dir = glob.glob(os.path.join(os.path.join(tmp_path, "*", "*", "*.*")), recursive=True)

        if not already_save_predicate :
            filter_list_dir = []
            for dir in self.list_dir :
                if extension.match(dir) :
                    filter_list_dir.append(dir)
            self.list_dir = filter_list_dir
        # SentencesBinariesWithSaveAndRegexGenerator.logger.debug("list of directory %s",  self.list_dir)
        SentencesBinariesWithSaveAndRegexGenerator.logger.debug("len of list of directory %s",  len(self.list_dir))
        if not already_save_predicate :
            p = re.compile(".*\.php")
            list = [ s for s in filter_list_dir if p.match(s) ]
            # self.list_dir += list
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Number of %s files in the generator %d", p, len(list))
            p = re.compile(".*\.js")
            list = [s for s in filter_list_dir if p.match(s)]
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Number of %s files in the generator %d", p, len(list))
            # self.list_dir += list
            p = re.compile(".*\.zip")
            list = [s for s in filter_list_dir if p.match(s)]
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Number of %s files in the generator %d", p, len(list))
            # self.list_dir += list
            p = re.compile(".*\.tar\.gz")
            list = [s for s in filter_list_dir if p.match(s)]
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Number of %s files in the generator %d", p, len(list))
            # self.list_dir += list
            p = re.compile(".*\.tgz")
            list = [s for s in filter_list_dir if p.match(s)]
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Number of %s files in the generator %d", p, len(list))

        # list = [os.path.getsize(s) for s in self.list_dir]
        # SentencesBinariesWithSaveAndRegexGenerator.logger.info("NOT sorted files %s", list)
        self.list_dir = sorted(self.list_dir, key=lambda M: os.path.getsize(M))
        list = [os.path.getsize(s) for s in self.list_dir]
        SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Size of the ascending sort files %s",self.list_dir.__len__())
        SentencesBinariesWithSaveAndRegexGenerator.logger.debug("List of the ascending sort files %s", self.list_dir)

        self.func = methodToConvertLineToListOfTokens
        self.save_predicate = save_predicate
        self.multifiles_predicate = multifiles_predicate

    def __iter__(self):
        SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Number of file in the directory %s", len(self.list_dir))
        for fname in self.list_dir:
            if self.already_save_predicate :
                SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Open the file in binary read mode %s", fname)
                if os.path.getsize(fname) > 0:
                    with open(fname, 'rb') as fp:
                        unpickler = pickle.Unpickler(fp);
                        res = unpickler.load()
                    SentencesBinariesWithSaveAndRegexGenerator.logger.debug("LOAD AND DESERIALIZE THE FILE FINISHED --> %s", fname)
                    SentencesBinariesWithSaveAndRegexGenerator.logger.debug("LEN of the list of token of %s --> %s", fname, len(res))
                else :
                    res = []
                yield res

            if self.save_predicate :
                tmp = Path(fname)
                binary_name_result = tmp.stem + '.bin'
                SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Original name = " + os.path.join(self.savePath,tmp.parts[-3], tmp.parts[-2], tmp.parts[-1]))
                SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Binary  name = " + binary_name_result)
                if self.multifiles_predicate :
                    path_result = FolderHelper.generatePathResult(tmp,self.savePath)
                    path_result_with_name = path_result / binary_name_result
                    SentencesBinariesWithSaveAndRegexGenerator.logger.debug("path_result  name = " + str(path_result))
                    SentencesBinariesWithSaveAndRegexGenerator.logger.debug("path_result_with_name  name = " + str(path_result_with_name))
                else :
                    middle_path = os.path.join(tmp.parts[-3], tmp.parts[-2])
                    path_result_with_name = Path(os.path.join(self.savePath, middle_path, binary_name_result))
                    path_result = Path(os.path.join(self.savePath, middle_path))
                SentencesBinariesWithSaveAndRegexGenerator.logger.debug("PATH RESULT %s", path_result_with_name)
                if not os.path.exists(path_result_with_name) :
                    SentencesBinariesWithSaveAndRegexGenerator.logger.debug("HAVE TO GENERATE AND SERIALIZE THE FILE %s AND THE SIZE OF THIS FILE %s",
                                 binary_name_result, os.path.getsize(tmp))
                    FolderHelper.create_folder_if_not_exist(path_result)

                    res = self.apply_func_and_save_serialize_file(fname, path_result_with_name)
                else :
                    SentencesBinariesWithSaveAndRegexGenerator.logger.debug("!!!!!!ALREADY SAVED JUST HAVE TO LOAD AND DESERIALIZE THE FILE --> %s ", binary_name_result)
                    if os.path.getsize(path_result_with_name) > 0:
                        with open(path_result_with_name, 'rb') as fp:
                            unpickler = pickle.Unpickler(fp);
                            res = unpickler.load()
                    else :
                        SentencesBinariesWithSaveAndRegexGenerator.logger.error("The file was empty -- we have to apply the fun and serialize the list of --> %s" , fname)
                        res = self.apply_func_and_save_serialize_file(fname, path_result_with_name)
            else :
                res = self.apply_func_and_save_serialize_file(fname, path_result_with_name, self.save_predicate)
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("LOAD AND DESERIALIZE THE FILE FINISHED --> %s", binary_name_result)
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("LEN of the list of token of %s --> %s", binary_name_result,  len(res))
            yield res

    def apply_func_and_save_serialize_file(self, fname, path_result, save_predicate=True):
        res = []
        SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Open the file in binary read mode %s", fname)
        for line in open(fname, 'rb'):
            if len(line) != 1:
                res = res + self.func(line)
                # SentencesBinariesWithSaveAndRegexGenerator.logger.debug("current res %s", res)
        # SentencesBinariesWithSaveAndRegexGenerator.logger.debug("current res %s", res)
        if save_predicate :
            SentencesBinariesWithSaveAndRegexGenerator.logger.debug("Have to save --> %s", path_result)
            with open(path_result, 'wb') as fp:
                mon_pickler = pickle.Pickler(fp)
                mon_pickler.dump(res)
        return res

class SentencesWithSave(StaticLoggingHelper):
    logger = logging.getLogger(__name__)
    def __init__(self, dirname, savePath):
        self.savePath = savePath
        self.dirname = dirname
        SentencesWithSave.logger.debug(dirname)
        self.list_dir = glob.glob(os.path.join(os.path.join(self.dirname, "*", "*", "*.*")), recursive=True)
        SentencesWithSave.logger.debug(os.path.join(self.dirname, "*", "*", "*.*"))
        SentencesWithSave.logger.debug("Number of %s files in the generator %d", ".js or .php", len(self.list_dir))


    def __iter__(self):
        SentencesWithSave.logger.debug("directory %s", self.list_dir)
        for fname in self.list_dir:
            res = []
            SentencesWithSave.logger.debug("fname %s", fname)
            for line in open(fname, 'r'):
                if len(line) != 1:
                    res = res + line.split()
            tmp = Path(fname)
            binary_name_result = tmp.stem + '.bin'
            SentencesWithSave.logger.debug("Original name = " + os.path.join(self.savePath,tmp.parts[-3], tmp.parts[-2], tmp.parts[-1]))
            SentencesWithSave.logger.debug("Binary  name = " + binary_name_result)
            path_result = Path(os.path.join(self.savePath, tmp.parts[-3], tmp.parts[-2], binary_name_result))
            if not os.path.exists(path_result) :
                FolderHelper.create_folder_if_not_exist(os.path.join(self.savePath, tmp.parts[-3], tmp.parts[-2]))
                SentencesWithSave.logger.debug("Have to save -- binary  name = " + binary_name_result)
                with open(path_result, 'wb') as fp:
                    SentencesWithSave.logger.debug("Open the file %s", fp)
                    mon_pickler = pickle.Pickler(fp)
                    mon_pickler.dump(res)
            else :
                SentencesWithSave.logger.debug("!!!!!!ALREADY SAVED -- binary  name = " + binary_name_result)
                pass
            yield res