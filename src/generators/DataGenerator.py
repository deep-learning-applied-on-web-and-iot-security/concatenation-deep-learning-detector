from pathlib import Path

from tensorflow import keras
import numpy as np
import os
import logging
class DataGenerator(keras.utils.Sequence) :
    'Generates data for Keras'

    def __init__(self, list_IDs, labels, vectorPath, batch_size=32, dim=(1,), n_channels=1,
                 n_classes=2, shuffle=True, seed=None ):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.vectorPath = vectorPath
        self.seed = seed
        self.on_epoch_end()


    def on_epoch_end(self):
        '''
        Here, the method on_epoch_end is triggered once at the very beginning as well as at the end of each epoch.
        If the shuffle parameter is set to True, we will get a new order of exploration at each pass
        (or just keep a linear exploration scheme otherwise).Here, the method on_epoch_end is triggered once at the very beginning as well as at the end of each epoch.
        If the shuffle parameter is set to True, we will get a new order of exploration at each pass
        (or just keep a linear exploration scheme otherwise).
        :return:
        '''
        'Updates indexes after each epoch'
        # logging.debug("list of ID to processed --> %s", self.list_IDs)
        logging.debug("len of list of ID to processed --> %d", len(self.list_IDs))
        self.indexes = np.arange(len(self.list_IDs))
        logging.debug("LEN OF indexes before to be shuffle %s", len(self.indexes))
        if self.shuffle == True:
            #logging.warning("SEEED %s", self.seed)
            if self.seed != None :
                np.random.seed(self.seed)
            np.random.shuffle(self.indexes)
            # logging.debug("indexes after to be shuffle %s", self.indexes)

    def __data_generation(self, list_IDs_temp):
        '''
        Another method that is core to the generation process is the one that achieves the most crucial job:
        producing batches of data.
        The private method in charge of this task is called __data_generation and takes as argument the list of IDs of the target batch.
        During data generation, this code reads the NumPy array of each example from its corresponding file ID.npy
        Since our code is multicore-friendly, note that you can do more complex operations instead
        (e.g. computations from source files) without worrying that data generation becomes a bottleneck in the training process.
        :param list_IDs_temp:
        :return:
        '''
        'Generates data containing batch_size samples'  # X : (n_samples, *dim, n_channels)
        # Initialization
        # logging.debug("Initialization of X and Y for %s -->", list_IDs_temp)
        len_list_ids_temp = len(list_IDs_temp)
        logging.debug("len(list_IDs_temp) --> %d",len_list_ids_temp)
        # logging.debug("len of this list %s -->", len(list_IDs_temp))
        if  len(list_IDs_temp) < self.batch_size :
            X = np.empty((len_list_ids_temp, self.dim))
            # logging.debug("the creation of the empty X --> %s", X)
            logging.debug("the shape of the empty X %s", X.shape)
            y = np.full((len_list_ids_temp), -1)
            # logging.debug("the creation of the empty Y --> %s", y)
            logging.debug("the shape of the empty Y %s", y.shape)
        else :
            X = np.empty((self.batch_size, self.dim))
            # logging.debug("the creation of the empty X --> %s", X)
            logging.debug("the shape of the empty X %s", X.shape)
            y = np.full((self.batch_size), -1)
            # logging.debug("the creation of the empty Y --> %s", y)
            logging.debug("the shape of the empty Y %s", y.shape)

        # Generate data
        # logging.debug("Begin to load the list IDs temp %s", list_IDs_temp)
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            # currentVector = np.load(os.path.join(self.vectorPath, ID + '.npy'))
            # logging.debug("the current vector loaded is %s", currentVector)
            # expand = np.expand_dims(currentVector, axis=0)
            # logging.debug("the current vector loaded is %s", expand)
            # logging.debug("Loading on the RAM the vector %s", os.path.join(self.vectorPath, ID + '.npy'))
            # logging.debug("list_IDs_temp=%s", list_IDs_temp)
            # logging.debug("vectorPath=%s", self.vectorPath)
            # logging.debug("ID=%s", ID)
            # logging.debug("VECTOR=%s",os.path.join(self.vectorPath, ID + '.npy'))
            # X[i,] = np.load(os.path.join(self.vectorPath, ID + '.npy')) #not compress
            #In this case, the savez_compressed() function supports saving multiple arrays to a single file.
            # Therefore, the load() function may load multiple arrays.
            #The loaded arrays are returned from the load() function in a dict with the names ‘arr_0’ for the first array,
            # ‘arr_1’ for the second, and so on.
            # logging.error("vector path %s,", self.vectorPath)
            # logging.error("ID of the vector %s,", ID)
            X[i,] = np.load(os.path.join(self.vectorPath, ID + '.npz'))['arr_0'] #load compressed vector
            # logging.debug("current state of the vector X %s -->", X)
            # logging.debug("shape of the vector X %s -->", X.shape)

            # Store classcreation
            # logging.debug("label of the file %s --> %d",ID, self.labels[ID] )
            #y[i] = self.labels[ID + '.php'] #I remove the extension php on the labels creation
            y[i] = self.labels[ID]
            # logging.debug("current state of the vector Y  --> %s", y)
            # logging.debug("shape of the vector Y -->  %s ", y.shape)

        #return X, keras.utils.to_categorical(y, num_classes=self.n_classes)
        # logging.debug("FINISH to load the list IDs temp")
        # logging.debug("X final %s", X)
        # logging.debug("Y final %s", y)
        y_transpose_and_expand = np.transpose(np.expand_dims(np.asarray(y), axis=0))
        # logging.debug("Y final %s",y_transpose_and_expand)
        # logging.debug("Y shape final %s",y_transpose_and_expand.shape)
        return X, y_transpose_and_expand

    def __len__(self):
        'Denotes the number of batches per epoch'
        # logging.debug("len of list ID %s",len(self.list_IDs))
        # logging.debug("batch_size %d", self.batch_size)
        # logging.debug("number of batches per epoch -- len of the generator %d", int(np.floor(len(self.list_IDs) / self.batch_size)))
        # return int(np.floor(len(self.list_IDs) / self.batch_size))
        return int((np.ceil(len(self.list_IDs) / self.batch_size)))

    def __getitem__(self, index):
        """
        Now, when the batch corresponding to a given index is called, the generator executes the __getitem__ method to generate it.
        :param index:
        :return:
        """
        'Generate one batch of data'
        # Generate indexes of the batch
        # logging.debug("Generation of the indexes")
        logging.debug("One batch data [ %d : %d [",index * self.batch_size,  (index + 1) * self.batch_size)
        logging.debug("end of the indexes %d", len(self.indexes))
        if ((index + 1) * self.batch_size) > len(self.indexes) : #we are in the end of the list
            # logging.info("HERRRREEE")
            indexes = self.indexes[index * self.batch_size:]
        else :
            indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # logging.debug("Generate indexes of the bath, %s", indexes)
        logging.debug("len of the indexes, %s", len(indexes))

        # Find list of IDs
        # list_IDs_temp = [self.list_IDs[k].split(".")[0] for k in indexes]
        list_IDs_temp = [self.list_IDs[k] for k in indexes]
        # logging.debug("list_IDs_temp len, %s", len(list_IDs_temp))
        # logging.debug("LIST ID TEMP len, %s", list_IDs_temp)

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)
        # logging.debug("generation of X %s", X)
        # logging.debug("generation of y %s", y)

        return X, y

    def getDimension(self):
        return self.dim

    def __str__(self):
        res = dict()
        for k,v in self.__dict__.items():
            if k == "labels" : continue
            res[k] = v
        return str(res)