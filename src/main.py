import getopt
import logging
import sys
from pprint import pprint

from src.managers.Manager import Manager

def usage() :
    print("TODO")

def main(argv):
    env = None
    config_ml = None
    try:
        opts, args = getopt.getopt(argv,"h:e:c",["env=","config="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-e", "--env"):
            env = arg
        elif opt in ("-c", "--config"):
            config_ml = arg
        else :
            usage()
            sys.exit(2)
    print("current env " + str(env))
    print("current config_ml " + str(config_ml))
    return env, config_ml

if __name__ == "__main__":
    logging.basicConfig(format="%(levelname)s - %(asctime)s - %(filename)s/%(funcName)s - line %(lineno)d: %(message)s",
                        datefmt='%H:%M:%S', level=logging.INFO)
    if len(sys.argv[1:]) > 0 :
        env, config_ml = main(sys.argv[1:])
        if not env :
            # env = "local_env.yaml"
            env = "local_debug_env.yaml"
            # env = "cluster_env.yaml"
            logging.info("Start with default env %s", env)
        else :
            logging.info("Start with env %s", env)
        if not config_ml :
            config_ml = "config_ml_detector.yaml"
            logging.info("Start with default conf ml %s", config_ml)
        else :
            logging.info("Start with conf ml %s", config_ml)
    else :
        env = "local_debug_env.yaml"
        config_ml = "config_ml_detector.yaml"




    logging.debug("Start")
    logging.warning("Start")
    logging.error("Start")
    manager = Manager(env, config_ml)
    config_env = manager.get_env()
    config_dbs = manager.get_dbs()

    print("--------------------------\n \tENVIRONMENT CONFIG \n--------------------------\n")
    pprint(config_env)
    print("--------------------------\n \tDBS CONFIG \n--------------------------\n")
    pprint(config_dbs)

    manager.settup_folders()
    manager.setup_launcher()
    manager.run_launcher()
