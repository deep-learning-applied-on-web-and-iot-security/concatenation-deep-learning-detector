import json
import logging

import pandas as pd
from openpyxl.styles import Font

from src.helpers.LoggingHelper import StaticLoggingHelper


class ResultDataframeFilesHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)

    @classmethod
    def save_in_json_file(cls, json_path, dict_result):
        with open(json_path, 'w') as json_file:
            json.dump(dict_result, json_file, indent=3)

    @classmethod
    def fill_row_dfs_in_existing_worksheet(cls, dict_res, list_df, dfs, embedding_size, list_keras_layers, xlsx_path,
                                               sheet_name):
        i = 0
        for df_name in list_df:
            cls.logger.debug("Current df_name --> %s ", df_name)
            cls.logger.debug("Current df_name %s embedding_size %s -->", df_name, embedding_size)
            columns = []
            for c in list_keras_layers:
                try:
                    if (dict_res[df_name][embedding_size][c] == None):
                        columns.append(0)
                    else:
                        columns.append(dict_res[df_name][embedding_size][c])
                except KeyError:
                    dict_res[df_name][embedding_size][c] = None
                    columns.append(None)
            dfs[i].loc[embedding_size] = columns
            dfs[i].caption = df_name
            cls.logger.debug("Fill the row %s --> %s", embedding_size, df_name)
            i += 1
        cls.positionning_dataframes_in_existing_worksheet(xlsx_path, dfs, sheet_name=sheet_name)

    @classmethod
    def positionning_dataframes_in_existing_worksheet(cls, xlsx_path, dfs, sheet_name) :
        i = 0
        startRow = 3
        while i < len(dfs):
            cls.logger.debug("write the current dfs on the file -->  %s", dfs[i])
            cls.append_df_to_excel(xlsx_path, dfs[i], sheet_name=sheet_name, startrow=startRow)
            if hasattr(dfs[i], "caption") :
                cls.append_df_caption(xlsx_path, dfs[i].caption, sheet_name, startRow, 1)

            if i + 1 < len(dfs):
                cls.append_df_to_excel(xlsx_path, dfs[i + 1], sheet_name=sheet_name, startrow=startRow, startcol= dfs[i].shape[1] + 4)
                if hasattr(dfs[i], "caption"):
                    cls.append_df_caption(xlsx_path, dfs[i + 1].caption, sheet_name, startRow, dfs[i].shape[1] + 4)
                # append_df_to_excel(path, dfs[i+1], sheet_name=sheet_name, startrow=startRow, startcol=11)

            startRow += dfs[i].shape[0] + 5  # number of rows # 5 because, to seperate properly the df
            # startRow +=11 #(fixed)
            i += 2

    @classmethod
    def append_df_to_excel(cls, filename, df, sheet_name='Sheet1', startrow=None, truncate_sheet=False, **to_excel_kwargs):
        """
        Append a DataFrame [df] to existing Excel file [filename]
        into [sheet_name] Sheet.
        If [filename] doesn't exist, then this function will create it.

        Parameters:
          filename : File path or existing ExcelWriter
                     (Example: '/path/to/file.xlsx')
          df : dataframe to save to workbook
          sheet_name : Name of sheet which will contain DataFrame.
                       (default: 'Sheet1')
          startrow : upper left cell row to dump data frame.
                     Per default (startrow=None) calculate the last row
                     in the existing DF and write to the next row...
          truncate_sheet : truncate (remove and recreate) [sheet_name]
                           before writing DataFrame to Excel file
          to_excel_kwargs : arguments which will be passed to `DataFrame.to_excel()`
                            [can be dictionary]

        Returns: None
        """
        from openpyxl import load_workbook
        # ignore [engine] parameter if it was passed
        if 'engine' in to_excel_kwargs:
            to_excel_kwargs.pop('engine')

        writer = pd.ExcelWriter(filename, engine='openpyxl')

        # Python 2.x: define [FileNotFoundError] exception if it doesn't exist
        try:
            FileNotFoundError
        except NameError:
            FileNotFoundError = IOError


        try:
            # try to open an existing workbook
            writer.book = load_workbook(filename)

            # get the last row in the existing Excel sheet
            # if it was not specified explicitly
            if startrow is None and sheet_name in writer.book.sheetnames:
                startrow = writer.book[sheet_name].max_row

            # truncate sheet
            if truncate_sheet and sheet_name in writer.book.sheetnames:
                # index of [sheet_name] sheet
                idx = writer.book.sheetnames.index(sheet_name)
                # remove [sheet_name]
                writer.book.remove(writer.book.worksheets[idx])
                # create an empty sheet [sheet_name] using old index
                writer.book.create_sheet(sheet_name, idx)

            # copy existing sheets
            writer.sheets = {ws.title:ws for ws in writer.book.worksheets}
        except FileNotFoundError:
            # file does not exist yet, we will create it
            pass

        if startrow is None:
            startrow = 0


        df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs)
        # if hasattr(df, 'caption') :
        #     cls.write_title(writer, sheet_name, df.caption, startrow -1 , **to_excel_kwargs)
        writer.save()

    @classmethod
    def write_title(cls, writer, sheet_name, title, startrow, startcol):
        worksheet = writer.sheets[sheet_name]
        cellref = worksheet.cell(startrow,  startcol)
        cellref.font = Font(name="Calibri", color="00000000", bold=True, size=16)
        if "df_" in title :
            res = title[3:]
        else :
            res = title
        if res[0].islower():
            res = res[0].capitalize() + res[1:]
        if "_" in res :
            res = res.split("_") #list
            res = " ".join(res)
        cellref.value = res



    @classmethod
    def createDataframes(cls, number, index, columns):

        result = []
        for i in range(number):
            result.append(pd.DataFrame(index=index, columns=columns))
        return result

    @classmethod
    def append_df_caption(cls, filename, caption, sheet_name, startrow, startcol):
        from openpyxl import load_workbook

        writer = pd.ExcelWriter(filename, engine='openpyxl')

        # Python 2.x: define [FileNotFoundError] exception if it doesn't exist
        try:
            FileNotFoundError
        except NameError:
            FileNotFoundError = IOError

        try:
            # try to open an existing workbook
            writer.book = load_workbook(filename)

            # get the last row in the existing Excel sheet
            # if it was not specified explicitly
            if startrow is None and sheet_name in writer.book.sheetnames:
                startrow = writer.book[sheet_name].max_row

            # copy existing sheets
            writer.sheets = {ws.title: ws for ws in writer.book.worksheets}
        except FileNotFoundError:
            # file does not exist yet, we will create it
            pass

        if startrow is None:
            startrow = 0

        cls.write_title(writer, sheet_name, caption, startrow - 1, startcol)
        writer.save()