import glob
import logging
import os
import pickle
import random
from pathlib import Path
from threading import Thread
import numpy as np

from src.generators.InputTokens import InputTokens
from src.helpers.DatasetHelper import DatasetHelper
from src.helpers.FolderHelper import FolderHelper
from src.helpers.LoggingHelper import StaticLoggingHelper

SAFE = "safe"
UNSAFE = "unsafe"
TOTAL_IN_DATASET = "total"
FILES = "files"
PATH = 'path'
TOTAL_FILES = 'totalFiles'

class VectorizationHelper(StaticLoggingHelper) :
    max_size_vector = None
    logger = logging.getLogger(__name__)
    @classmethod
    def createPaddingVectorsFor(cls, pathFile, pathResult, model,
                                sizeTokenVectors, multifiles_predicate=False):
        # max_number_of_token = getMaxOfNumberTokensInWholeDatabase(pathFile)
        cls.logger.debug("Begining of create padding vector by threading for %s", pathFile)
        list_dataset = glob.glob(os.path.join(pathFile, '*', ''), recursive=False)
        cls.logger.debug("the list of train + test + valid set : %s", list_dataset)

        cls.logger.debug("Création des threads")
        # Création des threads
        thread_1 = DefineMaxOfNumberTokensInADataset(list_dataset[0], multifiles_predicate)
        thread_2 = DefineMaxOfNumberTokensInADataset(list_dataset[1], multifiles_predicate)
        thread_3 = DefineMaxOfNumberTokensInADataset(list_dataset[2], multifiles_predicate)

        cls.logger.debug("Lancement des threads")
        # Lancement des threads
        thread_1.start()
        thread_2.start()
        thread_3.start()

        # Attend que les threads se terminent
        thread_1.join()
        thread_2.join()
        thread_3.join()
        cls.logger.debug("MAX of token on a train %d", thread_1.max_number_of_token)
        cls.logger.debug("MAX of token on a test %d", thread_2.max_number_of_token)
        cls.logger.debug("MAX of token on a valid %d", thread_3.max_number_of_token)
        max_number_of_token = max(thread_1.max_number_of_token, thread_2.max_number_of_token,
                                  thread_3.max_number_of_token)
        cls.logger.debug("MAX of token on the whole dataset %d", max_number_of_token)
        cls.logger.debug("MAX len of vector for one token --> %d", max_number_of_token * sizeTokenVectors)
        # cls.logger.info("MAX of token on the whole dataset %d", max_number_of_token)
        # cls.logger.info("MAX of sizeTokenVectors on the whole dataset %d", sizeTokenVectors)
        # cls.logger.info("MAX len of vector for one token --> %d", max_number_of_token * sizeTokenVectors)
        cls.logger.debug("Création des threads")
        # Création des threads
        thread_1 = CreatePaddingVectorForADataSet(list_dataset[0], pathResult, model, sizeTokenVectors,
                                                  max_number_of_token, multifiles_predicate)
        thread_2 = CreatePaddingVectorForADataSet(list_dataset[1], pathResult, model, sizeTokenVectors,
                                                  max_number_of_token, multifiles_predicate)
        thread_3 = CreatePaddingVectorForADataSet(list_dataset[2], pathResult, model, sizeTokenVectors,
                                                  max_number_of_token, multifiles_predicate)

        cls.logger.debug("Lancement des threads")
        # Lancement des threads
        thread_1.start()
        thread_2.start()
        thread_3.start()

        # Attend que les threads se terminent
        thread_1.join()
        thread_2.join()
        thread_3.join()
        assert thread_1.max_size_vector == thread_2.max_size_vector
        assert thread_1.max_size_vector == thread_3.max_size_vector
        cls.max_size_vector = thread_1.max_size_vector
        return thread_1.max_size_vector

    @classmethod
    def getMaxOfNumberTokensInWholeDatabase(cls, pathFile, multifiles_predicate=False):
        if multifiles_predicate :
            binaryPathFile = FolderHelper.get_list_dir_recursively(pathFile, format="str")
        else :
            binaryPathFile = glob.glob(pathFile + "*/*/*.bin", recursive=True)
        cls.logger.debug("number of file that we need to parse --> %d", len(binaryPathFile))
        maxOfNumberTokens = 0
        for file in binaryPathFile:
            file = open(file, 'rb')
            file = pickle.load(file)
            # cls.logger.debug(len(file))
            maxOfNumberTokens = max(maxOfNumberTokens, len(file))
            # cls.logger.debug("current max of token is %d", maxOfNumberTokens)

        return maxOfNumberTokens

    @classmethod
    def createPaddingVectorsForADataset(cls,pathFile, pathResult, model, sizeTokenVectors, max_number_of_token, multifiles_predicate=False):
        cls.logger.debug("MAX of token on the whole dataset --> %d", max_number_of_token)
        cls.logger.debug("Current path processing %s", pathFile)
        FolderHelper.create_folder_if_not_exist(pathResult)
        if multifiles_predicate :
            list_file = FolderHelper.get_list_dir_recursively(pathFile, format="str")
        else :
            list_file = glob.glob(os.path.join(pathFile, "*", "*.bin"), recursive=True)
        cls.logger.debug("number of file that we need to parse --> %d", len(list_file))
        max = max_number_of_token * sizeTokenVectors
        cls.logger.debug("max size of vector %d", max)
        for file in list_file:
            if (file == ".ipynb_checkpoints" or file == ".directory"): continue
            cls.logger.debug("Processing the file %s", file)
            tmp = Path(file)
            # tmpname = tmp.stem + '.npy'
            tmpname = tmp.stem + '.npz'
            cls.logger.debug("creation of the name of the file %s", tmpname)
            if multifiles_predicate :
                result_path = FolderHelper.generatePathResult(tmp, pathResult)
            else :
                result_path = os.path.join(pathResult, tmp.parts[-3], tmp.parts[-2])
            cls.logger.debug("path where save the file %s", result_path)
            result_name = os.path.join(result_path, tmpname)
            cls.logger.debug("final path root with name of file %s", result_name)
            if not os.path.exists(result_name):
                FolderHelper.create_folder_if_not_exist(result_path)
                cls.logger.debug("creation of the vector representation of the file %s", result_name)
                vector, numberOfTokens, sizeTokenVector = cls.getVectorRepresentationForABinaryFile(file, model)

                try:
                    assert max == (max_number_of_token * sizeTokenVector)
                except Exception as e:
                    cls.logger.error("expected %d BUT ACTUAL %d ==> generation of a vector with only 0", max, max_number_of_token * sizeTokenVector)
                    # raise e

                vector = cls.getPadding(vector, max)
                if max_number_of_token * sizeTokenVector == 0 :
                    cls.logger.error(f"CREATION VECTOR PADD WITH {max} * 0 DONE ")
                cls.logger.debug("saving vector on the folder %s", result_name)
                # np.save(result_name, vector)
                np.savez_compressed(result_name, vector)
            else:
                cls.logger.debug("Padding vector already created -- nothing to do")
        return max

    @classmethod
    def getPadding(cls,vector, max):
        cls.logger.debug("LEN VECTOR %s <=? MAX %s", len(vector), max)
        assert len(vector) <= max, "the len of the vector"
        return np.pad(vector, (0, (max - len(vector))), constant_values=0)

    @classmethod
    def getVectorRepresentationForABinaryFile(cls,binaryPathFile, model):
        file = open(binaryPathFile, 'rb')
        file = pickle.load(file)
        # cls.logger.debug(file)
        number_of_tokens = 0
        vector = np.array([])
        tokenVector = []
        for token in file:
            # cls.logger.debug(token)
            tokenVector = model.wv.get_vector(token)
            # cls.logger.debug("tokens --> %s --> transforming in vector %s (the first 3 is diplayed)", token, tokenVector[:3])
            vector = np.concatenate([vector, tokenVector])
            # cls.logger.debug("len of the concatenate tokens vectors %d", len(vector))
            number_of_tokens += 1
        return vector, number_of_tokens, len(tokenVector)

    @classmethod
    def getMaxOfNumberTokensForDataSet(cls, pathFile, multifiles_predicate=False):
        if multifiles_predicate :
            binaryPathFile = FolderHelper.get_list_dir_recursively(pathFile, format="str")
        else :
            binaryPathFile = glob.glob(os.path.join(pathFile, "*", "*.bin"), recursive=True)
        cls.logger.debug("number of file that we need to parse --> %d", len(binaryPathFile))
        maxOfNumberTokens = 0
        for file in binaryPathFile:
            file = open(file, 'rb')
            file = pickle.load(file)
            # cls.logger.debug(len(file))
            maxOfNumberTokens = max(maxOfNumberTokens, len(file))
            # cls.logger.debug("current max of token is %d", maxOfNumberTokens)
        # cls.logger.debug("max number tokens %d for --> %s", maxOfNumberTokens, pathFile)
        return maxOfNumberTokens

    @classmethod
    def get_XY_lists_from_dataset(cls, dataset):
        cls.logger.debug("Retrieve information about the whole dataset (safe, unsafe) %s", dataset)
        dataset_path = Path(dataset)
        informationsFiles = DatasetHelper.retrieve_informations_about_dataset(dataset_path)
        
        cls.logger.debug("Total of source codes/files in the whole dataset : %d ", informationsFiles[TOTAL_IN_DATASET])
        cls.logger.debug("Total of source codes/files in the safe dataset : %d", informationsFiles[SAFE][TOTAL_FILES])
        cls.logger.debug("Total of source codes/files in the unsafe dataset : %d", informationsFiles[UNSAFE][TOTAL_FILES])
        X_NameFileSafe, Y_safe, X_NameFileunsafe, Y_unsafe = cls.getXYSafeAndUnSafeSets(informationsFiles)
        cls.logger.debug("Creation of %d safe labels (0) corresponding to each safe file",
                     informationsFiles[SAFE][TOTAL_FILES])
        cls.logger.debug("Creation of %d unsafe labels (1) corresponding to each unsafe file",
                     informationsFiles[UNSAFE][TOTAL_FILES])
        cls.logger.debug("Shuffle the safe and unsafe file and shuffle in the same way the corresponding labels set ")
        X_NameFileList, Y_list = cls.shuffleDataSet(X_NameFileSafe, Y_safe, X_NameFileunsafe, Y_unsafe)
        cls.logger.debug("Len of shuffled the Name File list : %d -- Len if the shuffled labels list : %d",
                     len(X_NameFileList),
                     len(Y_list))
        cls.logger.debug("Showing the correspondance between the suffled lists")
        cls.logger.debug("X_NameFileList[0] -> %s -- safe? %s", X_NameFileList[0], Y_list[0])
        cls.logger.debug("X_NameFileList[1] -> %s -- safe? %s", X_NameFileList[1], Y_list[1])
        cls.logger.debug("X_NameFileList[2] -> %s -- safe? %s", X_NameFileList[2], Y_list[2])
        cls.logger.debug("X_NameFileList[2] -> %s -- safe? %s", X_NameFileList[3], Y_list[3])
        cls.logger.debug("X_NameFileList[2] -> %s -- safe? %s", X_NameFileList[4], Y_list[4])
        return X_NameFileList, Y_list, informationsFiles

    @classmethod
    def shuffleDataSet(cls, X_safe, Y_safe, X_unsafe, Y_unsafe):
        X_list = X_safe + X_unsafe
        Y_list = Y_safe + Y_unsafe
        mapIndexPosition = list(zip(X_list, Y_list))
        random.shuffle(mapIndexPosition)
        X_list, Y_list = zip(*mapIndexPosition)
        return X_list, Y_list

    @classmethod
    def getXYSafeAndUnSafeSets(cls, informationsFiles):
        return [os.path.join(informationsFiles[SAFE][PATH], file) for file in informationsFiles[SAFE][FILES]], \
               cls.getYSet(informationsFiles[SAFE][TOTAL_FILES], SAFE), \
               [os.path.join(informationsFiles[UNSAFE][PATH], file) for file in
                informationsFiles[UNSAFE][FILES]], cls.getYSet(informationsFiles[UNSAFE][TOTAL_FILES], UNSAFE)

    @classmethod
    def getYSet(cls,total, safe):
        return [0] * total if (safe == SAFE) else [1] * total

    @classmethod
    def getLists(cls, list_pathFile, model):
        """

        :param list_pathFile:
        :param model:
        :return: tuple of 3 list :
            - list of vectors representing each files
            - list of number of tokens in each files
            - list of size a vector representing each token
        """
        vectors = []
        tokens = []
        sizeTokenVectors = []
        for pathFile in list_pathFile:
            tmp = pathFile.split("/")
            if tmp[len(tmp) - 1] == ".directory": continue
            vector, numberOfTokens, sizeTokenVector = cls.getVectorRepresentationForAFile(pathFile, model)
            vectors.append(vector)
            tokens.append(numberOfTokens)
            sizeTokenVectors.append(sizeTokenVector)
        return vectors, tokens, sizeTokenVectors

    @classmethod
    def getVectorRepresentationForAFile(cls, pathFile, model):
        # logging.ino(pathFile)
        number_of_tokens = 0
        vector = np.array([])
        tokenVector = []
        generatorOfTokensFile = InputTokens(pathFile)
        for token in generatorOfTokensFile:
            cls.logger.debug("current token %s", token)
            tokenVector = model.wv.get_vector(token)
            # cls.logger.debug("tokens --> %s --> transforming in vector %s (the first 3 is diplayed)", token, tokenVector[:3])
            vector = np.concatenate([vector, tokenVector])
            number_of_tokens += 1
        cls.logger.debug("len of the concatenate tokens vectors %d", len(vector))
        return vector, number_of_tokens, len(tokenVector)


class DefineMaxOfNumberTokensInADataset(Thread):
    """"""

    def __init__(self, path_dataset, multifiles_predicate=False):
        Thread.__init__(self)
        self.list_of_file = path_dataset
        self.max_number_of_token = 0
        self.multifiles_predicate = multifiles_predicate

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        self.max_number_of_token = VectorizationHelper.getMaxOfNumberTokensForDataSet(self.list_of_file, self.multifiles_predicate)


class CreatePaddingVectorForADataSet(Thread):
    """"""

    def __init__(self, pathFile, pathResult, model, sizeTokenVectors, max_number_of_token, multifiles_predicate=False):
        Thread.__init__(self)
        self.pathFile = pathFile
        self.pathResult = pathResult
        self.model = model
        self.sizeTokenVectors = sizeTokenVectors
        self.max_number_of_token = max_number_of_token
        self.max_size_vector = 0
        self.multifiles_predicate = multifiles_predicate

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        self.max_size_vector = VectorizationHelper.createPaddingVectorsForADataset(self.pathFile, self.pathResult, self.model, self.sizeTokenVectors, self.max_number_of_token, self.multifiles_predicate)