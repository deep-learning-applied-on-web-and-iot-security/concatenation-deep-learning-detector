import logging
import os
from pathlib import Path

from src.helpers.LoggingHelper import StaticLoggingHelper

SAFE = "safe"
UNSAFE = "unsafe"
TOTAL_IN_DATASET = "total"
FILES = "files"
PATH = 'path'
TOTAL_FILES = 'totalFiles'

class DatasetHelper(StaticLoggingHelper):
    logger = logging.getLogger(__name__)

    @classmethod
    def retrieve_informations_about_dataset(cls,path):
        """
        @return : dictionnary such as :
        - key : absolute path of each folder contains in the folder path
        - el : dictionnary such as :
            - key : totalFiles
            - el : number of files in the folder corresponding to the key names
        """
        informationsFiles = {TOTAL_IN_DATASET: 0}
        cls.logger.debug("database path %s", path)
        for r, n, _ in os.walk(path):
            for name in n :
                path = Path(r, name)
                for root, _, files in os.walk(path):
                    lenFiles = len(files)
                    # cls.logger.debug("NUMBER OF FILES %s", len(files))
                    # cls.logger.debug("ROOT FILES %s", root)
                    # cls.logger.debug("name FILES %s", _)

                    informationsFiles = cls.setInformationsFiles(informationsFiles, root, lenFiles, files)
                    # cls.logger.debug("informationsFiles %s", informationsFiles)
                    informationsFiles = cls.setTotalInDataset(informationsFiles, lenFiles)
        return informationsFiles

    @classmethod
    def setInformationsFiles(cls,informationsFiles, root, lenFiles, files):
        """

        :param informationsFiles: dictionary to informations about the dataset
        :param root: root path of the folder
        :param lenFiles: number of files in the current folder
        :param files: list of files inside the current folder
        :return:
        """
        tmp = root.split('/')
        if SAFE in tmp:
            informationsFiles[SAFE] = {'totalFiles': lenFiles, 'unsafe?': 0, 'path': root, FILES: files}
        elif UNSAFE in tmp:
            informationsFiles[UNSAFE] = {'totalFiles': lenFiles, 'unsafe?': 1, 'path': root, FILES: files}
        return informationsFiles

    @classmethod
    def setTotalInDataset(cls, informationsFiles, lenFiles):
        """

        :param informationsFiles: dictionary of informations about the dataset
        :param lenFiles: number of files in the current folder
        :return: dictionary of informations modified with the total of files in the whole dataset
        """
        informationsFiles[TOTAL_IN_DATASET] = informationsFiles[TOTAL_IN_DATASET] + lenFiles
        return informationsFiles