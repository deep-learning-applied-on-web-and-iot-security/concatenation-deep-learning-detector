import logging
import os
from pathlib import Path

from src.helpers.LoggingHelper import StaticLoggingHelper


class FolderHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)


    @classmethod
    def create_folder_if_not_exist(cls, path) :
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)
            cls.logger.debug("%s doesn't exist ==> creation of this folder", path)
        else:
            cls.logger.debug("%s exist ==> do nothing", path)

    @classmethod
    def get_list_dir_recursively(cls, path, format=None):
        listeFichiers = []
        for (repertoire, sousRepertoires, fichiers) in os.walk(path):
            # print(f"{repertoire} {sousRepertoires} {fichiers}")
            if fichiers :
                if format == "str" :
                    fichiers_with_root = [Path(repertoire, f).__str__() for f in fichiers ]
                else :
                    fichiers_with_root = [Path(repertoire, f) for f in fichiers ]
                listeFichiers.extend(fichiers_with_root)
        return listeFichiers

    @classmethod
    def generatePathResult(cls, root_path_file_with_path_name, path_result):
        root_str = root_path_file_with_path_name.parent.__str__()
        if "trainset_files" in root_str :
            tmp = "trainset_files"
        elif "validset_files" in root_str :
            tmp = "validset_files"
        elif "testset_files" in root_str :
            tmp = "testset_files"
        else :
            raise Exception("the root path need to contains trainset, validset or testset_files")
        tmp = os.path.join(tmp, root_str.split(tmp+"/")[1])
        res = os.path.join(path_result, tmp)
        return Path(res)