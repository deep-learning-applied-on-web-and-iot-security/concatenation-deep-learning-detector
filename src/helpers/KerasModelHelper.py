import logging
import os
from tensorflow import keras as k
import tensorflow as tf

from src.helpers.FolderHelper import FolderHelper
from src.helpers.LoggingHelper import StaticLoggingHelper


class KerasModelHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)
    @classmethod
    def getKerasModel(cls, X_train, model_name, path,
                      keras_layers=None, save_keras_predicate=False):
        try:
            return cls.getKerasModelBy(model_name, path)
        except FileExistsError:
            return cls.createKerasModelWith(X_train, model_name=model_name, path=path,
                                        keras_layers=keras_layers, save_keras_predicate=save_keras_predicate)

    @classmethod
    def getKerasModelWithGenerator(cls,training_generator, model_name, path,
                                    keras_layers=None, save_keras_predicate=False):
        try:
            return cls.getKerasModelBy(model_name, path)
        except FileExistsError or ValueError:
            keras_model = cls.createKerasModelWithGenerator(training_generator, model_name=model_name, path=path,
                                                         keras_layers=keras_layers, save_keras_predicate=save_keras_predicate)
            return keras_model

    @classmethod
    def getKerasModelBy(cls, model_name, path):
        cls.logger.debug("Getting the model %s in the folder %s", model_name, path)
        path_model = os.path.join(path, model_name)
        if (os.path.exists(path_model)):
            cls.logger.debug("the model was previously built and trained --> can be load it")
            return k.models.load_model(path_model)
        else:
            raise FileExistsError(path_model, "doesn't exist ", "Create the model before to get it")

    @classmethod
    def createKerasModelWithGenerator(cls, training_generator, model_name, path,
                                       keras_layers=None, save_keras_predicate=False):
        # With `clear_session()` called at the beginning,
        # Keras starts with a blank state at each iteration
        # and memory consumption is constant over time.
        tf.keras.backend.clear_session()
        cls.logger.debug("Creating the model %s in the folder %s ", model_name, path)
        cls.logger.debug("ADD SHAPE INPUT %s ", (training_generator.getDimension(),))
        if keras_layers:
            cls.logger.debug("Have to generate these layers %s", keras_layers)
            input, output = cls.generateLayers(keras_layers, training_generator.dim)
        else:
            input = k.Input(shape=(training_generator.getDimension(),), dtype=tf.float32)
            # input = k.Input( shape=(None,None), dtype=tf.float32)
            cls.logger.debug("INPUT SHAPE " + str(input.shape))
            dense = k.layers.Dense(64, activation='relu')(input)
            dense = k.layers.Dense(128, activation='relu')(dense)
            # dense = k.layers.Dense(128, activation='relu')(dense)
            # dense = k.layers.Dense(256, activation='relu')(dense)
            # dense = k.layers.Dense(128, activation='relu')(dense)
            dense = k.layers.Dense(64, activation='relu')(dense)
            output = k.layers.Dense(1, activation='sigmoid')(dense)

        # Compile le model
        model = k.Model(inputs=input, outputs=output)
        model.summary()
        # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', tf.keras.metrics.Recall()])
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        if save_keras_predicate :
            path_model = os.path.join(path, model_name)
            FolderHelper.create_folder_if_not_exist(path)
            model.save(path_model)
            cls.logger.debug("SAVING MODEL -  DONE")
        return model

    @classmethod
    def generateLayers(cls,keras_layers, dim):
        keras_layers = keras_layers.split("-")
        # keras_layers = list(map(lambda i: int(i), keras_layers))
        input = k.Input(shape=(dim,), dtype=tf.float32)
        # dense = k.layers.Dense(int(keras_layers[0]), activation='relu')(input)
        if "d" in keras_layers[0]:
            dense = k.layers.Dropout(float(keras_layers[0][1:]))(input)
        elif "r1" in keras_layers[0]:
            dense = k.layers.Dense(int(keras_layers[0][:-2]), activation='relu',
                                   kernel_regularizer=tf.keras.regularizers.l1(l1=0.01))(input)
        elif "r2" in keras_layers[0]:
            keras_layers_temp = keras_layers[0].split("_")
            cls.logger.warning("penality : %s", float(keras_layers_temp[1][2:]))
            dense = k.layers.Dense(int(keras_layers_temp[0]), activation='relu',
                                   kernel_regularizer=tf.keras.regularizers.l2(float(keras_layers_temp[1][2:])))(input)
        else:
            dense = k.layers.Dense(int(keras_layers[0]), activation='relu')(input)
        for i in range(1, len(keras_layers) - 1):
            cls.logger.debug("i --> %s", i)
            if "d" in keras_layers[i]:
                dense = k.layers.Dropout(float(keras_layers[i][1:]))(dense)
            elif "r1" in keras_layers[i]:
                dense = k.layers.Dense(int(keras_layers[i][:-2]), activation='relu',
                                       kernel_regularizer=tf.keras.regularizers.l1(l1=0.001))(dense)
            elif "r2" in keras_layers[i]:
                keras_layers_temp = keras_layers[i].split("_")
                dense = k.layers.Dense(int(keras_layers_temp[0]), activation='relu',
                                       kernel_regularizer=tf.keras.regularizers.l2(float(keras_layers_temp[1][2:])))(
                    dense)
            else:
                # dense = k.layers.Dense(int(keras_layers[i]), activation='relu', kernel_regularizer=tf.keras.regularizers.l1_l2(l1=0.001, l2=0.001))(dense)
                # dense = k.layers.Dense(int(keras_layers[i]), activation='relu', kernel_regularizer=tf.keras.regularizers.l1_l2(l1=0.001, l2=0.001))(dense)
                dense = k.layers.Dense(int(keras_layers[i]), activation='relu')(dense)
        return input, k.layers.Dense(keras_layers[len(keras_layers) - 1], activation='sigmoid')(dense)

    @classmethod
    def createKerasModelWith(cls, X_train, model_name, path, keras_layers, save_keras_predicate=False):
        # With `clear_session()` called at the beginning,
        # Keras starts with a blank state at each iteration
        # and memory consumption is constant over time.
        tf.keras.backend.clear_session()
        cls.logger.debug("Creating the model %s in the folder %s ", model_name, path)
        cls.logger.debug("ADD SHAPE INPUT %s ", (X_train.shape[1],))
        if keras_layers:
            cls.logger.debug("Have to generate these layers %s", keras_layers)
            input, output = cls.generateLayers(keras_layers, X_train.shape[1])
        else:
            input = k.Input(shape=(X_train.shape[1],), dtype=tf.float32)
            # input = k.Input( shape=(None,None), dtype=tf.float32)
            cls.logger.debug("INPUT SHAPE " + str(input.shape))
            dense = k.layers.Dense(64, activation='relu')(input)
            dense = k.layers.Dense(128, activation='relu')(dense)
            # dense = k.layers.Dense(128, activation='relu')(dense)
            # dense = k.layers.Dense(256, activation='relu')(dense)
            # dense = k.layers.Dense(128, activation='relu')(dense)
            dense = k.layers.Dense(64, activation='relu')(dense)
            output = k.layers.Dense(1, activation='sigmoid')(dense)

        # Compile le model
        model = k.Model(inputs=input, outputs=output)
        model.summary()
        # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', tf.keras.metrics.Recall()])
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        if save_keras_predicate:
            path_model = os.path.join(path, model_name)
            FolderHelper.create_folder_if_not_exist(path)
            model.save(path_model)
            cls.logger.debug("SAVING MODEL -  DONE")
        return model