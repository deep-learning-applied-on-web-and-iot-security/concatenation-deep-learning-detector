import logging
from pprint import pprint


class StaticLoggingHelper(object):
    @classmethod
    def settup_logging(cls, config):
        debug_level = config.get_params_of(cls.__name__)["debug_level"]
        cls.logger.setLevel(logging.__getattribute__(debug_level))


class LoggingHelper(StaticLoggingHelper):

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def settup_logging(self, class_name):
        debug_level = self.config.get_params_of(class_name)["debug_level"]
        self.logger.setLevel(logging.__getattribute__(debug_level))