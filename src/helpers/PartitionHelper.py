import glob
import json
import logging
import os
import re
from pathlib import Path
from threading import Thread


from src.helpers.LoggingHelper import StaticLoggingHelper


class PartitionHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)
    @classmethod
    def generatePartitionAndLabelsForTheWholeDataset(cls,pathFile, multifiles_predicate=False, extension=False):
        LEN = "len"
        UNSAFE = 'unsafe'
        SAFE = 'safe'
        cls.logger.debug("Begining of create padding vector by threading")
        list_path_dataset = os.path.join(pathFile, '*')
        cls.logger.debug("What we have to parse %s", list_path_dataset)
        list_dataset = glob.glob(list_path_dataset, recursive=False)
        cls.logger.debug("the list of train + test + valid set : %s", list_dataset)
        dataset_1 = Path(list_dataset[0]).parts[-1]
        dataset_2 = Path(list_dataset[1]).parts[-1]
        dataset_3 = Path(list_dataset[2]).parts[-1]
        cls.logger.debug(dataset_3)
        partition = {dataset_1: [],
                     dataset_2: [],
                     dataset_3: []
                     }
        labels = {}
        information = {dataset_1: [],
                       dataset_2: [],
                       dataset_3: []}
        if type(extension) is str :
            extension = re.compile(extension)
        cls.logger.debug("Création des threads")
        # Création des threads
        thread_1 = ThreadGenerationPartitionAndLabelsForADataset(list_dataset[0], dataset_1, multifiles_predicate, extension)
        thread_2 = ThreadGenerationPartitionAndLabelsForADataset(list_dataset[1], dataset_2, multifiles_predicate, extension)
        thread_3 = ThreadGenerationPartitionAndLabelsForADataset(list_dataset[2], dataset_3, multifiles_predicate, extension)

        cls.logger.debug("Lancement des threads")
        # Lancement des threads
        thread_1.start()
        thread_2.start()
        thread_3.start()

        # Attend que les threads se terminent
        thread_1.join()
        thread_2.join()
        thread_3.join()

        partition.update(thread_1.partition)
        partition.update(thread_2.partition)
        partition.update(thread_3.partition)
        labels.update(thread_1.labels)
        labels.update(thread_2.labels)
        labels.update(thread_3.labels)
        information.update(thread_1.information)
        information.update(thread_2.information)
        information.update(thread_3.information)
        validation_unsafe_len = information[dataset_1][UNSAFE][LEN]
        validation_safe_len = information[dataset_1][SAFE][LEN]
        train_safe_len = information[dataset_2][SAFE][LEN]
        train_unsafe_len = information[dataset_2][UNSAFE][LEN]
        test_safe_len = information[dataset_3][SAFE][LEN]
        test_unsafe_len = information[dataset_3][UNSAFE][LEN]
        total_file = validation_safe_len + validation_unsafe_len + train_safe_len + train_unsafe_len + test_safe_len + test_unsafe_len
        information["total"] = total_file
        return partition, labels, information


    @classmethod
    def generatePartitionAndLabelsAndRulesForADataSet(cls, root_dataset_path, original_sanitization_path_json_file):
        """

        :param root_dataset_path: can not be a relative path...
        :return:
        """
        tmp = str(root_dataset_path).split("/")
        nameDataset = tmp[-1]
        logging.debug("generation of the partition and labels for %s", nameDataset)
        resPartition = {nameDataset: []}
        resLabels = {}
        resRules = {}
        information_dataset = {nameDataset: {}}
        for root, name, files in os.walk(root_dataset_path):
            if ".directory" in files:
                # logging.debug("REMOVE .DIRECTORY %s", files)
                files.remove(".directory")
                # logging.debug("REMOVE .DIRECTORY %s", files)
            if os.path.join(nameDataset, 'safe') in root:
                logging.debug("root" + root)
                files = list(map(lambda s: os.path.join(nameDataset, 'safe', s.split(".")[0]), files))
                logging.debug("number of files in safe %d", len(files))
                resLabels.update(cls.generateLabels(files, 1))
                resRules.update(cls.generateRules(files, original_sanitization_path_json_file))
                logging.debug("number of labels in safe %d", resLabels.__len__())
                information_dataset[nameDataset]["safe"] = {"len": len(files),
                                                            "root": os.path.join(nameDataset, 'safe'), "files": files}
            elif os.path.join(nameDataset, 'unsafe') in root:
                logging.debug("root" + root)
                files = list(map(lambda s: os.path.join(nameDataset, 'unsafe', s.split(".")[0]), files))
                logging.debug("number of files in unsafe %d", len(files))
                resLabels.update(cls.generateLabels(files, 0))
                logging.debug("number of labels in unsafe %d", resLabels.__len__())
                information_dataset[nameDataset]["unsafe"] = {"len": len(files),
                                                              "root": os.path.join(nameDataset, 'safe'), "files": files}
                resRules.update(cls.generateRules(files, original_sanitization_path_json_file))
            resPartition[nameDataset] += files
        return resPartition, resLabels, information_dataset, resRules

    @classmethod
    def generateRules(cls, files, original_sanitization_path_json_file):
        # logging.debug(files)
        # logging.debug(safe)
        res = {}
        for f in files:
            dict_rules = cls.find_the_rules_applied_from_the_file_name(f, original_sanitization_path_json_file)
            res[f] = dict_rules
        # logging.debug(res)
        return res

    @classmethod
    def find_the_rules_applied_from_the_file_name(cls, string, original_sanitization_path_json_file):

        sanitization_name_flush, sanitization_name = cls.find_the_sanitization_applied_from_the_file_name(string,
                                                                                                          original_sanitization_path_json_file)
        # print( get_rules_from(original_sanitization_path_json_file, sanitization_name))
        return cls.get_rules_from(original_sanitization_path_json_file, sanitization_name)

    @classmethod
    def get_rules_from(cls, original_sanitization_path_json_file, sanitization_name):

        with open(original_sanitization_path_json_file) as f:
            original_sanitization_json_data = json.load(f)
        if "sanitization" in str(original_sanitization_path_json_file):
            predication_sanitization_file = True
            res = {
                "rule0": "0",
                "rule1": "0",
                "rule2": "0",
                "rule3": "0",
                "rule4": "0",
                "rule5": "0"
            }
        else:
            predication_sanitization_file = False
            res = {}
        if predication_sanitization_file:
            # logging.debug(original_sanitization_json_data[sanitization_name]["safety"])
            res["rule0"] = cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule0")
            res["rule1"] =  cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule1")
            res["rule2"] =  cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule2")
            res["rule3"] =  cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule3")
            res["rule4"] =  cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule4")
            res["rule5"] =  cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule5")
        else:
            rule =  cls.try_find_key_in_dict(original_sanitization_json_data[sanitization_name]["safety"], "rule")
            res["rule"] = rule
        return res

    @classmethod
    def try_find_key_in_dict(cls, dict, key):
        try:
            res = dict[key]
        except KeyError as e:
            res = "0"
        return res

    @classmethod
    def find_the_sanitization_applied_from_the_file_name(cls, string, original_sanitization_path_json_file):
        with open(original_sanitization_path_json_file) as f:
            original_sanitization_json_data = json.load(f)
            if "sanitization" in str(original_sanitization_path_json_file):
                predication_sanitization_file = True
            else:
                predication_sanitization_file = False
        for sanitization_name in original_sanitization_json_data:
            # logging.debug("current sanitization flush digit number--> " + sanitization_name)
            sanitization_name_flush = re.sub(r'-[0-9]+', '', sanitization_name)
            # logging.debug("current sanitization --> " + sanitization_name_flush)
            # logging.debug("current string --> " + string)
            if sanitization_name_flush in string:
                if predication_sanitization_file:
                    if re.search("noFlow", string, re.IGNORECASE):
                        logging.debug("no flow in --> %s ", string)
                        if not re.search("noFlow", sanitization_name_flush, re.IGNORECASE):
                            logging.debug("NOT no flow in the --> %s HAVE TO CONTINUE TO SEARCH",
                                          sanitization_name_flush)
                            continue
                logging.debug("FOUNDED --> %s in %s", sanitization_name_flush, string)
                return sanitization_name_flush, sanitization_name
        return None  # Sanitization not found

    @classmethod
    def generateLabels(cls, files, safe):
        # cls.logger.debug(files)
        # cls.logger.debug(safe)
        res = {}
        if safe:
            for f in files:
                res[f] = 0
        else:
            for f in files:
                res[f] = 1
        # cls.logger.debug(res)
        return res

    @classmethod
    def generatePartitionAndLabelsForADataSet_original(cls, root_dataset_path, nameDataset):
        logging.debug("generation of the partition and labels for %s", nameDataset)
        resPartition = {nameDataset: []}
        resLabels = {}
        information_dataset = {nameDataset: {}}
        for root, name, files in os.walk(root_dataset_path):
            if ".directory" in files:
                files.remove(".directory")
            if os.path.join(nameDataset, 'safe') in root:
                logging.debug(f"root={root}")
                files = list(map(lambda s: os.path.join(nameDataset, 'safe', s.split(".")[0]), files))
                logging.debug("number of files in safe %d", len(files))
                resLabels.update(PartitionHelper.generateLabels(files, 1))
                logging.debug("number of labels in safe %d", resLabels.__len__())
                information_dataset[nameDataset]["safe"] = {"len": len(files),
                                                                 "root": os.path.join(nameDataset, 'safe'), "files": files}
                logging.debug(information_dataset)
            elif os.path.join(nameDataset, 'unsafe') in root:
                logging.debug(f"root={root}")
                files = list(map(lambda s: os.path.join(nameDataset, 'unsafe', s.split(".")[0]), files))
                logging.debug("number of files in unsafe %d", len(files))
                resLabels.update(PartitionHelper.generateLabels(files, 0))
                logging.debug("number of labels in unsafe %d", resLabels.__len__())
                information_dataset[nameDataset]["unsafe"] = {"len": len(files),
                                                                   "root": os.path.join(nameDataset, 'unsafe'), "files": files}
            resPartition[nameDataset] += files
        return resPartition, resLabels, information_dataset

    @classmethod
    def generatePartitionAndLabelsForADataSet(cls, root_dataset_path, nameDataset, multifiles_predicate=False, extension=False):
        logging.debug("generation of the partition and labels for %s", nameDataset)
        resPartition = {nameDataset: []}
        resLabels = {}
        information_dataset = {nameDataset: {}}
        if type(extension) is str :
            extension = re.compile(extension)
        for root, name, files in os.walk(root_dataset_path):
            if ".directory" in files:
                files.remove(".directory")
            if extension :
                filter_list_files = []
                for file in files:
                    if extension.match(file):
                        filter_list_files.append(file)
                files = filter_list_files
            if os.path.join(nameDataset, 'safe') in root:
                logging.debug(f"root={root}")
                logging.debug(f"root={name}")
                logging.debug(f"root={files}")
                if files.__len__() <= 0 : continue
                if multifiles_predicate :
                    path_result = root.split('/safe/')
                    path_result = path_result[1][:]
                    # files = list(map(lambda s: os.path.join(nameDataset, 'safe',path_result, s.split(".")[0]), files))
                    files = list(map(lambda s: os.path.join(nameDataset, 'safe',path_result, Path(s).stem), files))
                else :
                    files = list(map(lambda s: os.path.join(nameDataset, 'safe', Path(s).stem), files))
                logging.debug("number of files in safe %d", len(files))
                resLabels.update(PartitionHelper.generateLabels(files, 1))
                logging.debug("number of labels in safe %d", resLabels.__len__())
                # if multifiles_predicate :
                #     # root = os.path.join(nameDataset,'safe', path_result)
                # else :
                #     root =  os.path.join(nameDataset, 'safe')
                root =  os.path.join(nameDataset, 'safe')
                if multifiles_predicate :
                    if "safe" in information_dataset[nameDataset]:
                        information_dataset[nameDataset]["safe"]["len"] += len(files)
                        information_dataset[nameDataset]["safe"]["files"] += files
                    else :
                        information_dataset[nameDataset]["safe"] = {"len": len(files),
                                                                      "root": root, "files": files}
                logging.debug(information_dataset)
            elif os.path.join(nameDataset, 'unsafe') in root:
                logging.debug(f"root={root}")
                logging.debug(f"root={name}")
                logging.debug(f"root={files}")
                if multifiles_predicate and files.__len__() <= 0 :
                    continue
                if multifiles_predicate :
                    # path_result = root.split('/unsafe')
                    # path_result = path_result[1][1:]  #"remove the slasgh"
                    path_result = root.split('/unsafe/')
                    path_result = path_result[1][:]
                    # files = list(map(lambda s: os.path.join(nameDataset, 'unsafe',path_result, s.split(".")[0]), files))
                    files = list(map(lambda s: os.path.join(nameDataset, 'unsafe',path_result, Path(s).stem), files))
                else :
                    # files = list(map(lambda s: os.path.join(nameDataset, 'unsafe', s.split(".")[0]), files))
                    files = list(map(lambda s: os.path.join(nameDataset, 'unsafe', Path(s).stem), files))

                logging.debug("number of files in unsafe %d", len(files))
                resLabels.update(PartitionHelper.generateLabels(files, 0))
                logging.debug("number of labels in unsafe %d", resLabels.__len__())
                # if multifiles_predicate :
                #     root = os.path.join(nameDataset,'unsafe', path_result)
                # else :
                #     root =  os.path.join(nameDataset, 'unsafe')
                root =  os.path.join(nameDataset, 'unsafe')
                if multifiles_predicate :
                    if "unsafe" in information_dataset[nameDataset] :
                        information_dataset[nameDataset]["unsafe"]["len"] += len(files)
                        information_dataset[nameDataset]["unsafe"]["files"] += files
                    else :
                        information_dataset[nameDataset]["unsafe"] = {"len": len(files),
                                                                  "root": root, "files": files}
                else :
                    information_dataset[nameDataset]["unsafe"] = {"len": len(files),
                                                              "root": root, "files": files}
            resPartition[nameDataset] += files
        return resPartition, resLabels, information_dataset
#TODO LOGGING ISSUE  HERE
class ThreadGenerationPartitionAndLabelsForADataset(Thread) :
    """Thread chargé simplement d'afficher une lettre dans la console."""

    def __init__(self, root_dataset_path, nameDataset, multifiles_predicate=False, extension=False):
        Thread.__init__(self)
        self.root_dataset_path = root_dataset_path
        self.nameDataset = nameDataset
        self.partition = {}
        self.labels = {}
        self.multifiles_predicate = multifiles_predicate
        if type(extension) is str :
            self.extension = re.compile(extension)
        else :
            self.extension = extension

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        self.partition, self.labels, self.information = PartitionHelper.generatePartitionAndLabelsForADataSet(self.root_dataset_path, self.nameDataset, self.multifiles_predicate, self.extension)

    def generatePartitionAndLabelsForADataSet(self):
        logging.debug("generation of the partition and labels for %s", self.nameDataset)
        resPartition = {self.nameDataset: []}
        resLabels = {}
        information_dataset = {self.nameDataset: {}}
        for root, name, files in os.walk(self.root_dataset_path):
            if ".directory" in files:
                files.remove(".directory")
            if os.path.join(self.nameDataset, 'safe') in root:
                logging.debug("root" + root)
                files = list(map(lambda s: os.path.join(self.nameDataset, 'safe', s.split(".")[0]), files))
                logging.debug("number of files in safe %d", len(files))
                resLabels.update(PartitionHelper.generateLabels(files, 1))
                logging.debug("number of labels in safe %d", resLabels.__len__())
                information_dataset[self.nameDataset]["safe"] = {"len": len(files),
                                                            "root": os.path.join(self.nameDataset, 'safe'), "files": files}
                logging.debug(information_dataset)
            elif os.path.join(self.nameDataset, 'unsafe') in root:
                logging.debug("root" + root)
                files = list(map(lambda s: os.path.join(self.nameDataset, 'unsafe', s.split(".")[0]), files))
                logging.debug("number of files in unsafe %d", len(files))
                resLabels.update(PartitionHelper.generateLabels(files, 0))
                logging.debug("number of labels in unsafe %d", resLabels.__len__())
                information_dataset[self.nameDataset]["unsafe"] = {"len": len(files),
                                                              "root": os.path.join(self.nameDataset, 'unsafe'), "files": files}
            resPartition[self.nameDataset] += files
        return resPartition, resLabels, information_dataset

