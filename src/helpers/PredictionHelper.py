import logging
import multiprocessing
import os

import numpy as np
import pandas as pd

from src.helpers.LoggingHelper import StaticLoggingHelper
from src.helpers.PartitionHelper import PartitionHelper
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper


class PredictionHelper(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)

    @classmethod
    def getPredictionRegardingRules(cls,root_dataset_path, kerasModel, testing_generator,
                                    original_sanitization_path_json_file, y_test=None):
        cls.logger.debug(root_dataset_path)
        cls.logger.debug(original_sanitization_path_json_file)
        # root_dataset_path = Path(root_dataset_path)
        _, labels, _, resRules = PartitionHelper.generatePartitionAndLabelsAndRulesForADataSet(root_dataset_path,
                                                                               original_sanitization_path_json_file)
        cls.logger.debug("RES RULES %s", resRules)
        Y_pred = kerasModel.predict(testing_generator, workers=multiprocessing.cpu_count())
        y_pred = np.around(Y_pred).astype(int)  # Transform into int (0 or 1)
        if y_test is cls.getPredictionRegardingRules.__defaults__[0]:
            cls.logger.debug("cls.getPredictionRegardingRules.__defaults__ %s",cls.getPredictionRegardingRules.__defaults__)
            Y_TEST = np.concatenate([testing_generator[i][1] for i in range(len(testing_generator))])
        else :
            Y_TEST = y_test
        cls.logger.debug("Shape of Y_TEST (expected value) %s", Y_TEST)
        cls.logger.debug("Shape of Y_pred (predited value by keras %s", Y_pred.shape)
        cls.logger.debug("Shape of Y_TEST (expected value) %s", Y_TEST.shape)
        flatten = lambda l: [item for sublist in l for item in sublist]
        y_test_flatten = flatten(Y_TEST)
        y_pred_flatten = flatten(y_pred)
        if "sanitization" in str(original_sanitization_path_json_file):
            sanitization_file_predicate = True
        else:
            sanitization_file_predicate = False
        if sanitization_file_predicate:
            res_rule0, res_rule1, res_rule2, res_rule3, res_rule4, res_rule5 = cls.flatten_dict_rules(labels.keys(),
                                                                                                  resRules)
            dict_summarize = {"LABEL": list(labels.keys()), "Y_TEST": y_test_flatten, "Y_PRED": y_pred_flatten,
                              "RULE0": res_rule0, "RULE1": res_rule1, "RULE2": res_rule2, "RULE3": res_rule3,
                              "RULE4": res_rule4, "RULE5": res_rule5}
            cls.logger.warning(len(list(labels.keys())))
            cls.logger.warning(type((labels.keys())))
            cls.logger.warning(len(y_test_flatten))
            cls.logger.warning(type(y_test_flatten))
            cls.logger.warning(len(y_pred_flatten))
            cls.logger.warning(len(res_rule0))
            cls.logger.warning(type(res_rule0))
            cls.logger.warning(len(res_rule1))
            cls.logger.warning(len(res_rule2))
            cls.logger.warning(len(res_rule3))
            cls.logger.warning(len(res_rule4))
            cls.logger.warning(len(res_rule5))
        else:
            res_rule = cls.flatten_dict_rules(labels.keys(), resRules, sanitization_predicate=False)
            # cls.logger.warning("HEERREEEEEEEEE %s", res_rule)
            dict_summarize = {"LABEL": list(labels.keys()), "Y_TEST": y_test_flatten, "Y_PRED": y_pred_flatten,
                              "RULE": res_rule}
            if len(y_test_flatten) > len(y_pred_flatten) or len(y_test_flatten) > len(y_pred_flatten):
                cls.logger.error("ISSUE WITH THE BATCH SIZE OF THE GENERATOR Y_TEST %s != Y_PRED %s", len(y_test_flatten),
                              len(y_pred_flatten))
            else:
                cls.logger.debug("ISSUE WITH THE BATCH SIZE OF THE GENERATOR Y_TEST %s == Y_PRED %s", len(y_test_flatten),
                              len(y_pred_flatten))
        # df = pd.DataFrame.from_dict(dict_summarize)
        # cls.logger.warning(df)
        # return df
        return dict_summarize

    @classmethod
    def flatten_dict_rules(cls, list_of_key_labels, dict_rules, sanitization_predicate=True):
        if sanitization_predicate:
            res_rule0 = []
            res_rule1 = []
            res_rule2 = []
            res_rule3 = []
            res_rule4 = []
            res_rule5 = []
            for key_rules in dict_rules:  # keyrule = name of the file associated to dict rule
                for key_label in list_of_key_labels:  # key_label = name of the file associated to the label
                    if key_rules in key_label:
                        res_rule0.append(dict_rules[key_rules]["rule0"])
                        res_rule1.append(dict_rules[key_rules]["rule1"])
                        res_rule2.append(dict_rules[key_rules]["rule2"])
                        res_rule3.append(dict_rules[key_rules]["rule3"])
                        res_rule4.append(dict_rules[key_rules]["rule4"])
                        res_rule5.append(dict_rules[key_rules]["rule5"])
            return res_rule0, res_rule1, res_rule2, res_rule3, res_rule4, res_rule5
        else:
            res_rule = []
            for key_rules in dict_rules:
                for key_label in list_of_key_labels:
                    if key_rules in key_label:
                        res_rule.append(dict_rules[key_rules]["rule"])
            return res_rule

    @classmethod
    def saveDFPredictionForTestingResults(cls,df, XLSX, sheet_name):
        df = pd.DataFrame.from_dict(df)
        if os.path.exists(XLSX):
            ResultDataframeFilesHelper.append_df_to_excel(XLSX, df, sheet_name=sheet_name, startrow=1)
        else:
            writer = pd.ExcelWriter(XLSX, engine='xlsxwriter')
            startRow = 1
            df.to_excel(writer, sheet_name=sheet_name, startrow=startRow)
            writer.save()

    @classmethod
    def get_df_prediction_analyzer(cls, xlsx, sheet_name, model="construction"):
        dict_res_confusion_matrix, dict_res_metricx, dict_totals = cls.get_dict_prediction_analyzer(xlsx=xlsx,
                                                                                                sheet_name=sheet_name,
                                                                                                model=model)
        df_confusion_matrix = pd.DataFrame.from_dict(dict_res_confusion_matrix)
        df_metrix = pd.DataFrame.from_dict(dict_res_metricx)
        df_total = pd.DataFrame.from_dict(dict_totals)
        return df_confusion_matrix, df_metrix, df_total

    @classmethod
    def get_dict_prediction_analyzer(cls, xlsx, sheet_name, model="construction"):
        df_prediction = cls.get_df_prediction_test_on(xlsx, sheet_name, model=model)
        # logging.debug(df_prediction)
        # logging.debug(df_prediction.index)  # Name file
        # logging.debug(df_prediction.columns)  # Y_TEST Y PRED RULE
        dict_res_confusion_matrix = {}
        dict_res_metricx = {}
        dict_totals = {}

        for rule_number in range(0, 6):
            number_of_rule = cls.get_number_total_from(rule_number, df_prediction)
            total_safe = cls.get_number_total_safe_from(rule_number, df_prediction)
            total_unsafe = cls.get_number_total_unsafe_from(rule_number, df_prediction)
            number_of_rule_unsafe_predicted_unsafe_tp = cls.get_tp_from(rule_number, df_prediction)
            number_of_rule_safe_predicted_safe_tn = cls.get_tn_from(rule_number, df_prediction)
            number_of_rule_safe_predicted_safe_fp = cls.get_fp_from(rule_number, df_prediction)
            number_of_rule_safe_predicted_safe_fn = cls.get_fn_from(rule_number, df_prediction)
            key_rule = "rule" + str(rule_number)
            dict_res_confusion_matrix[key_rule] = {"TP": number_of_rule_unsafe_predicted_unsafe_tp,
                                                   "TN": number_of_rule_safe_predicted_safe_tn,
                                                   "FP": number_of_rule_safe_predicted_safe_fp,
                                                   "FN": number_of_rule_safe_predicted_safe_fn,
                                                   }
            dict_res_metricx[key_rule] = {"accuracy": cls.get_accuracy_from(rule_number, df_prediction),
                                          "precision": cls.get_precision_from(rule_number, df_prediction),
                                          "recall": cls.get_recall_from(rule_number, df_prediction),
                                          "TNR": cls.get_tnr_from(rule_number, df_prediction),
                                          "f_measure": cls.get_f_measure_from(rule_number, df_prediction),
                                          }
            dict_totals[key_rule] = {"total": number_of_rule,
                                     "total_safe": total_safe,
                                     "total_unsafe": total_unsafe}
        return dict_res_confusion_matrix, dict_res_metricx, dict_totals

    @classmethod
    def get_tp_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 1) & (
                    df_prediction["Y_PRED"] == 1)].shape[0]

    @classmethod
    def get_tn_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 0) & (
                    df_prediction["Y_PRED"] == 0)].shape[0]

    @classmethod
    def get_fp_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 0) & (
                    df_prediction["Y_PRED"] == 1)].shape[0]

    @classmethod
    def get_fn_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 1) & (
                    df_prediction["Y_PRED"] == 0)].shape[0]

    @classmethod
    def get_number_total_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number)].shape[0]

    @classmethod
    def get_number_total_safe_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 0)].shape[0]

    @classmethod
    def get_number_total_unsafe_from(cls, rule_number, df_prediction):
        return df_prediction.loc[(df_prediction["RULE"] == rule_number) & (df_prediction["Y_TEST"] == 1)].shape[0]

    @classmethod
    def get_accuracy_from(cls, rule_number, df_prediction):

        tp = cls.get_tp_from(rule_number, df_prediction)
        tn = cls.get_tn_from(rule_number, df_prediction)
        total = cls.get_number_total_from(rule_number, df_prediction)
        # logging.debug("tp=%s", tp)
        # logging.debug("tn=%s",tn)
        # logging.debug("total=%s",total)
        try:
            return (tp + tn) / total
        except ZeroDivisionError:
            return 0

    @classmethod
    def get_recall_from(cls, rule_number, df_prediction):
        tp = cls.get_tp_from(rule_number, df_prediction)
        fn = cls.get_fn_from(rule_number, df_prediction)
        try:
            return tp / (tp + fn)
        except ZeroDivisionError:
            return 0

    @classmethod
    def get_tnr_from(cls, rule_number, df_prediction):
        tn = cls.get_tn_from(rule_number, df_prediction)
        fp = cls.get_fp_from(rule_number, df_prediction)
        try:
            return tn / (tn + fp)
        except ZeroDivisionError:
            return 0

    @classmethod
    def get_f_measure_from(cls, rule_number, df_prediction):
        precision = cls.get_precision_from(rule_number, df_prediction)
        recall = cls.get_recall_from(rule_number, df_prediction)
        try:

            return 2 * precision * recall / (precision + recall)
        except ZeroDivisionError:
            return 0

    @classmethod
    def get_precision_from(cls, rule_number, df_prediction):
        tp = cls.get_tp_from(rule_number, df_prediction)
        fp = cls.get_fp_from(rule_number, df_prediction)
        try:
            return tp / (tp + fp)
        except ZeroDivisionError:
            return 0

    @classmethod
    def save_df_prediction_analyzer(cls, df_confusion_matrix, df_metrix, df_total, xlsx, sheet_name):
        if os.path.exists(xlsx):
            ResultDataframeFilesHelper.append_df_to_excel(xlsx, df_confusion_matrix, sheet_name=sheet_name, startrow=2)
            ResultDataframeFilesHelper.append_df_to_excel(xlsx, df_metrix, sheet_name=sheet_name, startrow=9)
            ResultDataframeFilesHelper.append_df_to_excel(xlsx, df_total, sheet_name=sheet_name, startrow=19)
        else:
            writer = pd.ExcelWriter(xlsx, engine='xlsxwriter')
            startRow = 1
            df_confusion_matrix.to_excel(writer, sheet_name=sheet_name, startrow=startRow)
            df_metrix.to_excel(writer, sheet_name=sheet_name, startrow=9)
            df_total.to_excel(writer, sheet_name=sheet_name, startrow=19)
            writer.save()

    @classmethod
    def get_df_prediction_test_on(cls, xlsx_path_file, sheet_name, nrows=6495, model=None):
        if model == "construction":
            skiprows = 1
            nrows = nrows
            usecols = np.arange(1, 5)  # +2 because of nodejs withe the extension max contexts
            index_col = 0
        else:
            skiprows = 1
            nrows = nrows
            usecols = np.arange(1, 10)  # +2 because of nodejs withe the extension max contexts
            index_col = 0
        return pd.read_excel(xlsx_path_file, sheet_name=sheet_name, skiprows=skiprows, nrows=nrows, usecols=usecols,
                             index_col=index_col, engine='openpyxl')





