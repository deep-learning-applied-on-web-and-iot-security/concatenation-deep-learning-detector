
import logging

from src.configurations.Config import Config
from src.detectors.Detectors import XSSConcatenationDetector, XSSGeneratorConcatenationDetector
from src.generators.SentencesGenerator import SentencesWithSave
from src.helpers.DatasetHelper import DatasetHelper
from src.helpers.FolderHelper import FolderHelper
from src.helpers.KerasModelHelper import KerasModelHelper
from src.helpers.LineCodeHelper import LineCodeHelper
from src.helpers.LoggingHelper import StaticLoggingHelper
from src.helpers.PartitionHelper import PartitionHelper
from src.helpers.PredictionHelper import PredictionHelper
from src.helpers.TokenizationDatasetHelper import TokenizationDatasetHelper
from src.helpers.VectorizationHelper import VectorizationHelper
from src.launchers.Launchers import PreprocessingLauncher, DefaultLauncher
from src.helpers.ResultDataframeFilesHelper import ResultDataframeFilesHelper


class Manager(StaticLoggingHelper) :
    logger = logging.getLogger(__name__)
    def __init__(self, env_yaml_path, config_yaml):
        self.launcher = None
        self.config = Config(env_yaml_path, config_yaml)
        for k, v in self.config.get_params_of(Manager.__name__).items() :
            self.__setattr__(k, v)
        Manager.logger.setLevel(self.debug_level)
        self.current_env = self.config.get_env()
        self.current_dbs = self.config.get_dbs()
        self.setup_logs()


    def get_env(self):
        return self.current_env

    def get_lang(self):
        return self.lang

    def get_db_version(self):
        return self.config.__getattribute__("db_version")

    def get_dbs(self):
        return self.current_dbs

    def settup_folders(self):
        path_result_dfs = self.current_env["result_dfs_path"]
        FolderHelper.create_folder_if_not_exist(path_result_dfs) #FIXED

    def setup_launcher(self):

        if self.multi_processing_predicate:
            Manager.logger.info("Call multiprocessing runner")
            self.launcher = eval(PreprocessingLauncher.__name__)(self.config)
        else:
            Manager.logger.info("Call default runner")
            self.launcher = eval(DefaultLauncher.__name__)(self.config)
        self.setup_concatenation_detector()


    def setup_logs(self):
        # GenericLauncher.settup_logging(debug_level["launcher_debug_level"])

        ResultDataframeFilesHelper.settup_logging(self.config)
        FolderHelper.settup_logging(self.config)
        LineCodeHelper.settup_logging(self.config)
        TokenizationDatasetHelper.settup_logging(self.config)
        VectorizationHelper.settup_logging(self.config)
        PartitionHelper.settup_logging(self.config)
        KerasModelHelper.settup_logging(self.config)
        PredictionHelper.settup_logging(self.config)
        DatasetHelper.settup_logging(self.config)
        SentencesWithSave.settup_logging(self.config)


    def setup_concatenation_detector(self):
        generator = self.generator_usage_predicate
        if generator:
            Manager.logger.info("Use runner with generator")
            self.launcher.set_detector(XSSGeneratorConcatenationDetector.__name__)
        else:
            Manager.logger.warning(
                "Use runner without generator -- you need to have enough RAM in your computer to load big vectors")
            self.launcher.set_detector(XSSConcatenationDetector.__name__)

    def run_launcher(self):
        Manager.logger.info("Begining to launch launcher on %s dbs", len(self.current_dbs))
        for lang in self.current_dbs :
            for key_db in self.current_dbs[lang]:
                if self.current_dbs[lang][key_db]["embedding_sizes"]:
                    args = dict()
                    args.update(self.current_dbs[lang][key_db])
                    args.update(self.current_env)
                    args.update({"lang" : lang})
                    args.update({"db_version": "db-" +self.get_db_version()[0:2]})
                    Manager.logger.debug("DBS + ENV ARGS %s,", args)
                    self.launcher.setup(**args)
                    self.launcher.launch()
            if self.test_best_models_predicate:
                best_dbs = self.config.setup_dbs(True)
                for key_db in self.current_dbs[lang]:
                    args = dict()
                    args.update(best_dbs[lang][key_db])
                    args.update(self.current_env)
                    args.update({"lang": lang})
                    args.update({"db_version": "db-" +self.get_db_version()[0:2]})
                    Manager.logger.debug("TEST BEST MODEL ARGS %s", args)
                    self.launcher.setup(**args)
                    self.launcher.launch()





