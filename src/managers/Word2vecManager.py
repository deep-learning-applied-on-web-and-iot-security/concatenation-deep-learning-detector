import logging
import multiprocessing
import os

from gensim.models import Word2Vec

from src.helpers.FolderHelper import FolderHelper


class Word2vecManager :
    @classmethod
    def getWord2VecModel(cls,**kwargs):
        try:
            return cls.getWord2VecModelBy(model_name=kwargs["model_name"], path=kwargs["model_path"])
        except FileExistsError:
            return cls.createWord2VecModelWith(**kwargs)

    @classmethod
    def getWord2VecModelBy(cls, model_name, path):
        logging.info("Getting the model %s in the folder %s", model_name, path)
        path_model = os.path.join(path, model_name)
        if (os.path.exists(path_model)):
            logging.info("the model was previously built and trained --> can be load it")
            return Word2Vec.load(path_model)
        else:
            raise FileExistsError(path_model, "doesn't exist ", "Create the model before to get it")

    @classmethod
    def createWord2VecModelWith(cls, **kwargs):
        model_name = kwargs["model_name"]
        model_path = kwargs["model_path"]
        CORES = multiprocessing.cpu_count()
        WORKERS = CORES - 1
        logging.info(
            "Creating the model %s in the folder %s --> but need to check if there is an new sentences generator",
            kwargs["model_name"], kwargs["model_path"])
        path_model = os.path.join(model_path, model_name)
        logging.info("the path model to create word2vec %s", model_path)
        FolderHelper.create_folder_if_not_exist(model_path)
        w2v_model = Word2Vec(min_count=kwargs["min_count"], window=kwargs["window"], vector_size=kwargs["size"], sample=kwargs["sample"], alpha=kwargs["sample"],
                             min_alpha=kwargs["min_alpha"],
                             negative=kwargs["negative"], workers=WORKERS)
        w2v_model.build_vocab(kwargs["sentences_generator"], progress_per=kwargs["progress_per"])
        TOTAL_EXAMPLES = w2v_model.corpus_count
        logging.info("Build the vocab - DONE %s",TOTAL_EXAMPLES)
        w2v_model.train(kwargs["sentences_generator"], total_examples=TOTAL_EXAMPLES, epochs=kwargs["epochs"], report_delay=kwargs["report_delay"])
        logging.info("Training the model - DONE")
        if kwargs.__contains__('save_word2vec_model_predicate') and kwargs["save_word2vec_model_predicate"] :
            w2v_model.save(path_model)
            logging.info("Saving the model in the folder %s --> done", path_model)

        return w2v_model
