# Statically identifying XSS using deep learning -- Concatenation technique (NLP)
The concatenation technique is an approach that represents the source code based on the tokenization of its contents.
This project is an official implementation of this approach described in:

[Héloïse Maurel](https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/), [Santiago Vidal](https://sites.google.com/site/santiagoavidal/) and [Tamara Rezk](https://www-sop.inria.fr/everest/Tamara.Rezk/),
"code2vec: Learning Distributed Representations of Code", POPL'2019 [[PDF]](https://urialon.cswp.cs.technion.ac.il/wp-content/uploads/sites/83/2018/12/code2vec-popl19.pdf)

_**April 2021** - The paper was accepted to [SECRYPT 2021](http://www.secrypt.org/?y=2021)_.

_**June 2021** - The talk video will be available soon<!--is available [here]()-->_.


Table of Contents
=================
  * [Requirements](#requirements)
  * [Quickstart](#quickstart)
  * [Configuration](#configuration)
  * [Features](#features)
  * [Currently languages support](#currently-languages-support)
  * [Extending to other languages](#extending-to-other-languages)
  * [Additional datasets](#additional-datasets)
  * [Citation](#citation)

## Requirements
On Linux (Ubuntu or Fedora):
  * [Python3](https://www.linuxbabe.com/ubuntu/install-python-3-6-ubuntu-16-04-16-10-17-04) (>=3.6). 
  To check the version:
```bash
python3 --version
```
  * If you are using a GPU, you will need CUDA - [download](https://developer.nvidia.com/) 


## Quickstart
### Step 0: Cloning this repository
```
git clone https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/statically-identifying-xss-using-deep-learning/concatenation-detector
cd concatenation-detector
```
### Step 1: Install dependencies by using virtualenv and `requirements.txt`

```bash
python3.6 -m venv venv
source venv/bin/activate
# inside (venv) 
pip install -r requirements.txt

```


### Step 2: Creating a new dataset from any sources
To have a dataset to train a network on, you can either cloning and run our extended generator
or create a new dataset of your own.


#### Cloning our extended generator 
```
git clone https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/statically-identifying-xss-using-deep-learning/vulnerability-generator-database
cd vulnerability-generator-database
```
Follow the instructions in the generator [README.md](https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/statically-identifying-xss-using-deep-learning/vulnerability-generator-database/-/blob/master/README.md)
This generator will generate safe and unsafe samples in separate directories, namely safe and unsafe, respectively.

#### Split the database in training, testing and validation dataset
To split the generator database :
  * Edit the file [preprocess.py](preprocess.py) using the instructions, pointing it to the correct safe and unsafe directories.
  * Run the preprocess.sh file:
> python preprocess.py

### Step 3: Training a model
You can either download an already-trained model or train a new model using a preprocessed dataset.
#### Training a model from scratch

To train a model from scratch:
  * Edit the file [train.sh](train.sh) to point it to the right preprocessed data. By default, 
  it points to our "java14m" dataset that was preprocessed in the previous step.
  * Before training, you can edit the configuration hyper-parameters in the file [config.py](config.py),
  as explained in [Configuration](#configuration).
  * Run the [train.sh](train.sh) script:
```
source train.sh
```

#### Downloading a trained model 
We already trained a model for eight epochs on the data that was preprocessed in the previous step.
The number of epochs was chosen using [early stopping](https://en.wikipedia.org/wiki/Early_stopping) as the version that maximized the F1 score on the validation set. This model can be downloaded [here](https://s3.amazonaws.com/code2vec/model/java14m_model.tar.gz) or using:
```
git clone https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/statically-identifying-xss-using-deep-learning/db-concatenation-detector
cd db-concatenation-detector
```


##### Notes:
  1. By default, the network is evaluated on the validation set after every training epoch.
  2. By default, the network is training for 20 epochs.
These settings can be changed by simply editing the file [local_env.yaml](local_env.yaml).



### Step 4: Evaluating a trained model


### Step 4: Manual examination of a trained model
To manually examine a trained model, run:


## Configuration
Changing hyper-parameters and other parameters are possible by editing both YAML files.
[local_env.yaml](local_env.yaml) and [config_ml_detector.yaml](config_ml_detector.yaml) .

Here are some of the parameters and their description:


## Features
Concatenation supports the following features: 

### Choosing implementation to use
This repository comes with four combinations of the model implementations:

1. uses Keras generator
2. uses numpy vectors. The particularity of this implementation is you need enough RAM to load the vectors.
3. run the detector with mulprocessing thread
4. run the detector without mulprocessing thread


### Exporting the trained token vectors and target vectors
Token and target embeddings are available to download: 

[[Token vectors]](https://s3.amazonaws.com/code2vec/model/token_vecs.tar.gz) [[Method name vectors]](https://s3.amazonaws.com/code2vec/model/target_vecs.tar.gz)

These saved embeddings are saved without subtoken-delimiters ("*toLower*" is saved as "*tolower*").

In order to export embeddings from a trained model, use the "--save_w2v" and "--save_t2v" flags:

Exporting the trained *token* embeddings:
```
python3 code2vec.py --load models/java14_model/saved_model_iter8.release --save_w2v models/java14_model/tokens.txt
```
Exporting the trained *target* (method name) embeddings:
```
python3 code2vec.py --load models/java14_model/saved_model_iter8.release --save_t2v models/java14_model/targets.txt
```
This saves the tokens/targets embedding matrices in word2vec format to the specified text file, in which:
the first line is: \<vocab_size\> \<dimension\>
and each of the following lines contains: \<word\> \<float_1\> \<float_2\> ... \<float_dimension\>

These word2vec files can be manually parsed or easily loaded and inspected using the [gensim](https://radimrehurek.com/gensim/models/word2vec.html) python package:

## Currently languages support

This project currently supports PHP and Node.js (with HTML and javascript) as the input languages.

## Extending to other languages  

More extensions will be available soon.

To extend the concatenation technique to work with other languages, you don't have to implement anything.
Theoretically, the implementation of this technique could use in any language.


## Citation

[Statically Identifying XSS using Deep Learning](https://gitlab.inria.fr/deep-learning-applied-on-web-and-iot-security/statically-identifying-xss-using-deep-learning/paper/-/blob/master/statically-identifying-xss-using-deep-learning-secrypt2021.pdf)

It will be available soon.
<!--
```
@article{maurel2021StaticallyIdentifyingXSSUsingDeepLearning,
 author = {Héloïse Maurel, Santiago Vidal, Tamara Rezk},
 title = {Statically Identifying XSS using Deep Learning},
 journal = {},
 issue_date = {April 2021},
 volume = {},
 number = {},
 month = apr,
 year = {2021},
 issn = {},
 pages = {},
 articleno = {},
 numpages = {},
 url = {},
 doi = {},
 acmid = {},
 publisher = {},
 address = {},
 keywords = {Web security, Deep learning, Web attacks, Cross-Site Scripting},
}
```
-->
